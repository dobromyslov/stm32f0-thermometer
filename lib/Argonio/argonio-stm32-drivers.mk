ARGONIO_STM32_DRIVERS = ./lib/Argonio/argonio-stm32-drivers

CSRC += $(ARGONIO_STM32_DRIVERS)/Delay/Delay.c \
		$(ARGONIO_STM32_DRIVERS)/LCM1602/LCM1602.c \
        $(ARGONIO_STM32_DRIVERS)/OneWire/OneWire.c \
        $(ARGONIO_STM32_DRIVERS)/DS18B20/DS18B20.c \
        $(ARGONIO_STM32_DRIVERS)/Thermometer/Thermometer.c \
        $(ARGONIO_STM32_DRIVERS)/MatrixKeyboard/MatrixKeyboard.c \
        $(ARGONIO_STM32_DRIVERS)/MatrixKeyboard/MatrixKeyboard_4x4_Type1.c \
        $(ARGONIO_STM32_DRIVERS)/Relay/Relay.c \
        $(ARGONIO_STM32_DRIVERS)/stm32f0xx-hal-eeprom-emu/stm32f0xx_hal_eeprom_emu.c \
        $(ARGONIO_STM32_DRIVERS)/StringUtils/StringUtils.c \
        $(ARGONIO_STM32_DRIVERS)/Console/Console.c

INCDIR += $(ARGONIO_STM32_DRIVERS)/Delay \
		  $(ARGONIO_STM32_DRIVERS)/LCM1602 \
          $(ARGONIO_STM32_DRIVERS)/OneWire \
          $(ARGONIO_STM32_DRIVERS)/DS18B20 \
          $(ARGONIO_STM32_DRIVERS)/Thermometer \
          $(ARGONIO_STM32_DRIVERS)/MatrixKeyboard \
          $(ARGONIO_STM32_DRIVERS)/Relay \
          $(ARGONIO_STM32_DRIVERS)/stm32f0xx-hal-eeprom-emu \
          $(ARGONIO_STM32_DRIVERS)/StringUtils \
          $(ARGONIO_STM32_DRIVERS)/Console
          