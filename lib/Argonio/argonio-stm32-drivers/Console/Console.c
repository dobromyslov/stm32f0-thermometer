/**
 * Console.c
 */

#include "Console.h"

UART_HandleTypeDef *huart;

void Console_init(UART_HandleTypeDef *_huart) {
    huart = _huart;
}

void Console_writeChar(char *c) {
    if (HAL_UART_Transmit(huart, (uint8_t*)c, 1, 1000) != HAL_OK) {
        Error_Handler();
    }
}

void Console_writeString(char *str) {
    if (HAL_UART_Transmit(huart, (uint8_t*)str, strlen(str), 1000) != HAL_OK) {
        Error_Handler();
    }
}

void Console_writeLine(char *str) {
    Console_writeString(str);
    Console_writeString("\r\n");
}

void Console_log(char *message) {
    Console_writeLine(message);
}
