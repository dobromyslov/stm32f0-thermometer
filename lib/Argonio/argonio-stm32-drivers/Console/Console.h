/**
 * Console.h
 */

#ifndef SRC_CONSOLE_H_
#define SRC_CONSOLE_H_

#include "stm32f0xx.h"

void Console_init(UART_HandleTypeDef *_huart);
void Console_writeChar(char *c);
void Console_writeString(char *str);
void Console_writeLine(char *str);
void Console_log(char *message);

#endif /* SRC_CONSOLE_H_ */
