
#include "Relay.h"

static Relay_Device_t *relays;
static uint8_t relaysCount;

#define GPIO_MODE             ((uint32_t)0x00000003)
#define GPIO_OUTPUT_TYPE      ((uint32_t)0x00000010)

void Relay_Init(Relay_Device_t *_relays, uint8_t _relaysCount) {
    uint8_t i;
    uint32_t temp;

    relays = _relays;
    relaysCount = _relaysCount;

    // Initialize GPIO for each relay control pin
    for (i = 0; i < relaysCount; i++) {
        Relay_Switch(&relays[i], 0);

        /* Configure IO Direction mode (Input, Output, Alternate or Analog) */
        temp = relays[i].port->MODER;
        temp &= ~(GPIO_MODER_MODER0 << (relays[i].pinNumber * 2));
        temp |= ((GPIO_MODE_OUTPUT_OD & GPIO_MODE) << (relays[i].pinNumber * 2));
        relays[i].port->MODER = temp;

        /* Configure the IO Speed */
        temp = relays[i].port->OSPEEDR;
        temp &= ~(GPIO_OSPEEDER_OSPEEDR0 << (relays[i].pinNumber * 2));
        temp |= (GPIO_SPEED_HIGH << (relays[i].pinNumber * 2));
        relays[i].port->OSPEEDR = temp;

        /* Configure the IO Output Type */
        temp = relays[i].port->OTYPER;
        temp &= ~(GPIO_OTYPER_OT_0 << relays[i].pinNumber) ;
        temp |= (((GPIO_MODE_OUTPUT_OD & GPIO_OUTPUT_TYPE) >> 4) << relays[i].pinNumber);
        relays[i].port->OTYPER = temp;

        /* Activate the Pull-up or Pull down resistor for the current IO */
        temp = relays[i].port->PUPDR;
        temp &= ~(GPIO_PUPDR_PUPDR0 << (relays[i].pinNumber * 2));
        temp |= (GPIO_NOPULL << (relays[i].pinNumber * 2));
        relays[i].port->PUPDR = temp;
    }
}

void Relay_Switch(Relay_Device_t *relay, uint8_t state) {
    if (state) {
        relay->port->BSRRH = relay->pin;
    }
    else {
        relay->port->BSRRL = relay->pin;
    }
}
