/**
 * Relay control.
 */

#ifndef STM32F0_THERMOMETER_RELAY_H
#define STM32F0_THERMOMETER_RELAY_H

#include "argonio_stm32_drivers_conf.h"

typedef struct {
    GPIO_TypeDef * port;
    uint16_t pin;
    uint8_t pinNumber;
} Relay_Device_t;

void Relay_Init(Relay_Device_t *_relays, uint8_t _relaysCount);
void Relay_Switch(Relay_Device_t *relay, uint8_t state);

#endif //STM32F0_THERMOMETER_RELAY_H
