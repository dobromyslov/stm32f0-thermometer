/**
 * ds18b20.c
 *
 */

#include "DS18B20.h"

#include "argonio_stm32_drivers_conf.h"

uint8_t DS18B20_ConvertT(OneWire_Device_t *device) {
    if (!OneWire_ResetAndDetectPresence(device->bus)) {
        return 0;
    }

    if (device->rom == NULL) {
        OneWire_SkipRom(device->bus);
    }
    else {
        OneWire_MatchRom(device->bus, device->rom);
    }
    OneWire_WriteByte(device->bus, DS18B20_CMD_CONVERT_T);

    return 1;
}

DS18B20_GetTResult_t DS18B20_GetT(OneWire_Device_t *device) {
    DS18B20_GetTResult_t result = {
        .value = DS18B20_TEMPERATURE_UNDEFINED,
        .error = DS18B20_GETT_OK
    };

    uint8_t measure = 0;

    if (!OneWire_ResetAndDetectPresence(device->bus)) {
        result.error = DS18B20_GETT_ERROR_NO_PRENSENCE;
        return result;
    }

    if (device->rom == NULL) {
        OneWire_SkipRom(device->bus);
    }
    else {
        OneWire_MatchRom(device->bus, device->rom);
    }
    OneWire_WriteByte(device->bus, DS18B20_CMD_READ_SCRATCHPAD);

    measure = OneWire_ReadByte(device->bus);
    measure |= OneWire_ReadByte(device->bus) << 8;

    result.value = measure * 0.0625;

    return result;
}

