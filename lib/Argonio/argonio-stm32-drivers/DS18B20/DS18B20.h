/**
 * ds18b20.h
 */

#ifndef DS18B20_H_
#define DS18B20_H_

#include <float.h>
#include "../OneWire/OneWire.h"

/**
 * DS18B20 1-wire family's code.
 */
#define DS18B20_FAMILY_CODE                 0x28

/**
 * DS18B20 commands.
 */
#define DS18B20_CMD_CONVERT_T               0x44
#define DS18B20_CMD_WRITE_SCRATCHPAD        0x4E
#define DS18B20_CMD_READ_SCRATCHPAD         0xBE
#define DS18B20_CMD_COPY_SCRATCHPAD         0x48
#define DS18B20_CMD_RECALL_E                0xB8

#define DS18B20_CMD_READ_POWER_SUPPLY       0xB4

/**
 * Undefined temperature value.
 */
#define DS18B20_TEMPERATURE_UNDEFINED       (-FLT_MAX)

typedef enum {
    DS18B20_GETT_OK = 0,
    DS18B20_GETT_ERROR_NO_PRENSENCE
} DS18B20_GetTError_t;

typedef struct {
    float value;
    DS18B20_GetTError_t error;
} DS18B20_GetTResult_t;

/**
 * Starts temperature conversion.
 * Returns 1 if 1-Wire bus present. 0 if no presence detected.
 */
uint8_t DS18B20_ConvertT(OneWire_Device_t *device);

/**
 * Gets temperature from the sensor.
 */
DS18B20_GetTResult_t DS18B20_GetT(OneWire_Device_t *device);

#endif /* DS18B20_H_ */
