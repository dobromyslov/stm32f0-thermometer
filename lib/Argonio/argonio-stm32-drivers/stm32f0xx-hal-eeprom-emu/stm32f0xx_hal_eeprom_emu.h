/**
  ******************************************************************************
  * @file    STM32F0xx_EEPROM_Emulation/inc/eeprom.h
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    29-May-2012
  * @brief   This file contains all the functions prototypes for the EEPROM
  *          emulation firmware library.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EEPROM_H
#define __EEPROM_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Exported constants --------------------------------------------------------*/
/* Define the size of the sectors to be used */
#define HAL_EEPROMEMU_PAGE_SIZE             ((uint32_t)0x0400)  /* Page size = 1KByte */

/**
 * EEPROM start address in Flash.
 *
 * Use page 62 and page 63 for EEPROM emulation.
 */
#define HAL_EEPROMEMU_START_ADDRESS  ((uint32_t)0x0800F800)

/* Variables' number */
#define HAL_EEPROMEMU_VARIABLES_COUNT       ((uint8_t)4)

/* No valid page define */
#define HAL_EEPROMEMU_NO_VALID_PAGE         ((uint8_t)0xAB)

/* Page full define */
#define HAL_EEPROMEMU_PAGE_FULL             ((uint8_t)0x80)

/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
uint16_t HAL_EEPROMEMU_Init(void);
uint16_t HAL_EEPROMEMU_ReadVariable(uint8_t virtAddress, uint16_t* data);
uint16_t HAL_EEPROMEMU_WriteVariable(uint8_t virtAddress, uint16_t data);
uint16_t HAL_EEPROMEMU_UpdateVariable(uint8_t virtAddress, uint16_t data);

#endif /* __EEPROM_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
