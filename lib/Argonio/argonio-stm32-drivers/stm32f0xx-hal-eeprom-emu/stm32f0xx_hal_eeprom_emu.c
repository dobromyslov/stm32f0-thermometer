/**
  ******************************************************************************
  * @file    STM32F0xx_EEPROM_Emulation/src/eeprom.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    29-May-2012
  * @brief   This file provides all the EEPROM emulation firmware functions.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/** @addtogroup STM32F0xx_EEPROM_Emulation
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal_eeprom_emu.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define PAGES_COUNT     ((uint8_t)0x02)

/* Page status definitions */
#define PAGE_STATUS_ERASED           ((uint16_t)0xFFFF)     /* Page is empty */
#define PAGE_STATUS_RECEIVE_DATA     ((uint16_t)0xEEEE)     /* Page is marked to receive data */
#define PAGE_STATUS_VALID            ((uint16_t)0x0000)     /* Page containing valid data */

/* Valid pages in read and write defines */
#define READ_FROM_VALID_PAGE  ((uint8_t)0x00)
#define WRITE_IN_VALID_PAGE   ((uint8_t)0x01)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Adresses for each used page */
uint16_t* pagePointers[PAGES_COUNT];

uint8_t page0 = 0;
uint8_t page1 = 1;
uint16_t* pagePointer0;
uint16_t* pagePointer1;

/* Global variable used to store variable value in read sequence */
uint16_t dataVar = 0;

/* Virtual address defined by the user: 0xFFFF value is prohibited */
uint8_t virtAddVarTab[HAL_EEPROMEMU_VARIABLES_COUNT];

/* Flash Erase Configuration */
FLASH_EraseInitTypeDef eraseConfig = {
    TYPEERASE_PAGES,
    0,
    1
};

uint32_t erasePageError;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
static HAL_StatusTypeDef EE_Format(void);
static uint16_t EE_VerifyPageFullWriteVariable(uint8_t virtAddress, uint16_t data);
static uint16_t EE_TransferPageWriteVariable(uint8_t virtAddress, uint16_t data);
static uint8_t EE_FindValidPage(uint8_t operation);
static uint16_t EE_TransferPage(uint16_t* src, uint16_t* dst);

/**
  * @brief  Restore the pages to a known good state in case of page's status
  *   corruption after a power loss.
  * @param  None.
  * @retval - Flash error code: on write Flash error
  *         - HAL_OK: on success
  */
uint16_t HAL_EEPROMEMU_Init(void)
{
    uint16_t pageStatus0, pageStatus1;
    HAL_StatusTypeDef flashStatus;

    flashStatus = HAL_FLASH_Unlock();
    if (flashStatus != HAL_OK) {
        return flashStatus;
    }

    uint8_t i;
    for (i = 0; i < HAL_EEPROMEMU_VARIABLES_COUNT; i++) {
        virtAddVarTab[i] = i;
    }

    for (i = 0; i < PAGES_COUNT; i++) {
        pagePointers[i] = (uint16_t*)(HAL_EEPROMEMU_START_ADDRESS + i * HAL_EEPROMEMU_PAGE_SIZE);
    }

    pagePointer0 = pagePointers[page0];
    pagePointer1 = pagePointers[page1];

    /* Get Page0 status */
    pageStatus0 = *pagePointer0;
    /* Get Page1 status */
    pageStatus1 = *pagePointer1;

    /* Check for invalid header states and repair if necessary */
    switch (pageStatus0) {
        case PAGE_STATUS_ERASED:
            /* Page0 erased, Page1 valid */
            if (pageStatus1 == PAGE_STATUS_VALID) {
                /* Erase Page0 */
                eraseConfig.PageAddress = (uint32_t)pagePointer0;
                return HAL_FLASHEx_Erase(&eraseConfig, &erasePageError);
            }
            /* Page0 erased, Page1 receive */
            else if (pageStatus1 == PAGE_STATUS_RECEIVE_DATA) {
                /* Erase Page0 */
                eraseConfig.PageAddress = (uint32_t)pagePointer0;
                flashStatus = HAL_FLASHEx_Erase(&eraseConfig, &erasePageError);
                if (flashStatus != HAL_OK) {
                    return flashStatus;
                }
                /* Mark Page1 as valid */
                return HAL_FLASH_Program(TYPEPROGRAM_HALFWORD, (uint32_t)pagePointer1, PAGE_STATUS_VALID);
            }
            /* First EEPROM access (Page0&1 are erased) or invalid state -> format EEPROM */
            else {
                /* Erase both Page0 and Page1 and set Page0 as valid page */
                return EE_Format();
            }
            break;

        case PAGE_STATUS_RECEIVE_DATA:
            /* Page0 receive, Page1 valid */
            if (pageStatus1 == PAGE_STATUS_VALID) {
                /* Transfer data from Page1 to Page0 */
                return EE_TransferPage(pagePointer1, pagePointer0);
            }
            /* Page0 receive, Page1 erased */
            else if (pageStatus1 == PAGE_STATUS_ERASED) {
                /* Erase Page1 */
                eraseConfig.PageAddress = (uint32_t)pagePointer1;
                flashStatus = HAL_FLASHEx_Erase(&eraseConfig, &erasePageError);
                if (flashStatus != HAL_OK) {
                    return flashStatus;
                }
                /* Mark Page0 as valid */
                return HAL_FLASH_Program(TYPEPROGRAM_HALFWORD, (uint32_t)pagePointer0, PAGE_STATUS_VALID);
            }
            /* Invalid state -> format eeprom */
            else {
                /* Erase both Page0 and Page1 and set Page0 as valid page */
                return EE_Format();
            }
            break;

        case PAGE_STATUS_VALID:
            /* Invalid state -> format eeprom */
            if (pageStatus1 == PAGE_STATUS_VALID) {
                /* Erase both Page0 and Page1 and set Page0 as valid page */
                return EE_Format();
            }
            /* Page0 valid, Page1 erased */
            else if (pageStatus1 == PAGE_STATUS_ERASED) {
                /* Erase Page1 */
                eraseConfig.PageAddress = (uint32_t)pagePointer1;
                return HAL_FLASHEx_Erase(&eraseConfig, &erasePageError);
            }
            /* Page0 valid, Page1 receive */
            else {
                /* Transfer data from Page0 to Page1 */
                return EE_TransferPage(pagePointer0, pagePointer1);
            }
            break;

        default:  /* Any other state -> format eeprom */
            /* Erase both Page0 and Page1 and set Page0 as valid page */
            return EE_Format();
    }

    return flashStatus;
}

/**
  * @brief  Returns the last stored variable data, if found, which correspond to
  *   the passed virtual address
  * @param  VirtAddress: Variable virtual address
  * @param  Data: Global variable contains the read variable value
  * @retval Success or error status:
  *           - 0: if variable was found
  *           - 1: if the variable was not found
  *           - NO_VALID_PAGE: if no valid page was found.
  */
uint16_t HAL_EEPROMEMU_ReadVariable(uint8_t virtAddress, uint16_t* data)
{
    uint8_t validPage;
    uint16_t addressValue, readStatus = 1;
    uint16_t *address, *pageStart;

    /* Get active Page for read operation */
    validPage = EE_FindValidPage(READ_FROM_VALID_PAGE);

    /* Check if there is no valid page */
    if (validPage == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return  HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    /* Get the valid Page start Address */
    pageStart = pagePointers[validPage];

    /* Get the valid Page end Address */
    address = pageStart + HAL_EEPROMEMU_PAGE_SIZE/2 - 1;//(uint32_t)((EEPROM_START_ADDRESS - 2) + (uint32_t)((1 + ValidPage) * PAGE_SIZE));

    /* Check each active page address starting from end */
    while (address > pageStart + 1) {
        /* Get the current location content to be compared with virtual address */
        addressValue = *address;

        /* Compare the read address with the virtual address */
        if (addressValue == virtAddress) {
            /* Get content of address-2 which is variable value */
            *data = *(address - 1);
            /* In case variable value is read, reset ReadStatus flag */
            readStatus = 0;
            break;
        }
        else {
            /* Next address location */
            address -= 2;
        }
    }

    /* Return ReadStatus value: (0: variable exist, 1: variable doesn't exist) */
    return readStatus;
}

/**
  * @brief  Writes/upadtes variable data in EEPROM.
  * @param  VirtAddress: Variable virtual address
  * @param  Data: 16 bit data to be written
  * @retval Success or error status:
  *           - FLASH_COMPLETE: on success
  *           - PAGE_FULL: if valid page is full
  *           - NO_VALID_PAGE: if no valid page was found
  *           - Flash error code: on write Flash error
  */
uint16_t HAL_EEPROMEMU_WriteVariable(uint8_t virtAddress, uint16_t data)
{
    /* Write the variable virtual address and value in the EEPROM */
    uint16_t status = EE_VerifyPageFullWriteVariable(virtAddress, data);

    if (status == HAL_EEPROMEMU_PAGE_FULL) {
        /* Perform Page transfer */
        status = EE_TransferPageWriteVariable(virtAddress, data);
    }

    /* Return last operation status */
    return status;
}

uint16_t HAL_EEPROMEMU_UpdateVariable(uint8_t virtAddress, uint16_t data) {
    uint16_t oldData;
    uint16_t readStatus = HAL_EEPROMEMU_ReadVariable(virtAddress, &oldData);
    if (readStatus == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    if (readStatus == 1 || oldData != data) {
        return HAL_EEPROMEMU_WriteVariable(virtAddress, data);
    }
    else {
        return HAL_OK;
    }
}

/**
  * @brief  Erases PAGE0 and PAGE1 and writes VALID_PAGE header to PAGE0
  * @param  None
  * @retval Status of the last operation (Flash write or erase) done during
  *         EEPROM formating
  */
static HAL_StatusTypeDef EE_Format(void)
{
    HAL_StatusTypeDef flashStatus = HAL_OK;

    /* Erase Page0 */
    eraseConfig.PageAddress = (uint32_t)pagePointer0;
    flashStatus = HAL_FLASHEx_Erase(&eraseConfig, &erasePageError);
    if (flashStatus != HAL_OK) {
        return flashStatus;
    }

    /* Set Page0 as valid page: Write VALID_PAGE at Page0 base address */
    flashStatus = HAL_FLASH_Program(TYPEPROGRAM_HALFWORD, (uint32_t)pagePointer0, PAGE_STATUS_VALID);
    if (flashStatus != HAL_OK) {
        return flashStatus;
    }

    /* Erase Page1 */
    eraseConfig.PageAddress = (uint32_t)pagePointer1;
    return HAL_FLASHEx_Erase(&eraseConfig, &erasePageError);
}

/**
  * @brief  Find valid Page for write or read operation
  * @param  Operation: operation to achieve on the valid page.
  *   This parameter can be one of the following values:
  *     @arg READ_FROM_VALID_PAGE: read operation from valid page
  *     @arg WRITE_IN_VALID_PAGE: write operation from valid page
  * @retval Valid page number (PAGE0 or PAGE1) or NO_VALID_PAGE in case
  *   of no valid page was found
  */
static uint8_t EE_FindValidPage(uint8_t operation)
{
    uint16_t pageStatus0, pageStatus1;

    /* Get Page0 actual status */
    pageStatus0 = *pagePointer0;

    /* Get Page1 actual status */
    pageStatus1 = *pagePointer1;

    /* Write or read operation */
    switch (operation) {
        case WRITE_IN_VALID_PAGE:   /* ---- Write operation ---- */
            if (pageStatus1 == PAGE_STATUS_VALID) {
                /* Page0 receiving data */
                if (pageStatus0 == PAGE_STATUS_RECEIVE_DATA) {
                    return page0;         /* Page0 valid */
                }
                else {
                    return page1;         /* Page1 valid */
                }
            }
            else if (pageStatus0 == PAGE_STATUS_VALID) {
                /* Page1 receiving data */
                if (pageStatus1 == PAGE_STATUS_RECEIVE_DATA) {
                    return page1;         /* Page1 valid */
                }
                else {
                    return page0;         /* Page0 valid */
                }
            }
            else {
                return HAL_EEPROMEMU_NO_VALID_PAGE;   /* No valid Page */
            }

        case READ_FROM_VALID_PAGE:  /* ---- Read operation ---- */
            if (pageStatus0 == PAGE_STATUS_VALID) {
                return page0;           /* Page0 valid */
            }
            else if (pageStatus1 == PAGE_STATUS_VALID) {
                return page1;           /* Page1 valid */
            }
            else {
                return HAL_EEPROMEMU_NO_VALID_PAGE;  /* No valid Page */
            }

        default:
            return page0;             /* Page0 valid */
    }
}

static uint16_t EE_TransferPage(uint16_t* src, uint16_t* dst)
{
    HAL_StatusTypeDef flashStatus;
    uint16_t varIdx, readStatus, eepromStatus;
    int16_t x = -1;

    /* Transfer data from source page to destination page */
    for (varIdx = 0; varIdx < HAL_EEPROMEMU_VARIABLES_COUNT; varIdx++) {
        if (( *(dst + 3)) == virtAddVarTab[varIdx]) {
            x = varIdx;
        }
        if (varIdx != x) {
            /* Read the last variables' updates */
            readStatus = HAL_EEPROMEMU_ReadVariable(virtAddVarTab[varIdx], &dataVar);
            /* In case variable corresponding to the virtual address was found */
            if (readStatus != 0x1) {
                /* Transfer the variable to the Page0 */
                eepromStatus = EE_VerifyPageFullWriteVariable(virtAddVarTab[varIdx], dataVar);
                if (eepromStatus != HAL_OK) {
                    return eepromStatus;
                }
            }
        }
    }
    /* Mark destination page as valid */
    flashStatus = HAL_FLASH_Program(TYPEPROGRAM_HALFWORD, (uint32_t)dst, PAGE_STATUS_VALID);
    if (flashStatus != HAL_OK) {
        return (uint16_t)flashStatus;
    }
    /* Erase source page */
    eraseConfig.PageAddress = (uint32_t)src;
    return (uint16_t)HAL_FLASHEx_Erase(&eraseConfig, &erasePageError);
}

/**
  * @brief  Verify if active page is full and Writes variable in EEPROM.
  * @param  VirtAddress: 16 bit virtual address of the variable
  * @param  Data: 16 bit data to be written as variable value
  * @retval Success or error status:
  *           - HAL_OK: on success
  *           - HAL_EEPROMEMU_PAGE_FULL: if valid page is full
  *           - HAL_EEPROMEMU_NO_VALID_PAGE: if no valid page was found
  *           - HAL error code: on write Flash error
  */
static uint16_t EE_VerifyPageFullWriteVariable(uint8_t virtAddress, uint16_t data)
{
    HAL_StatusTypeDef flashStatus = HAL_OK;
    uint8_t validPage;
    uint16_t *address, *pageEndAddress;

    /* Get valid Page for write operation */
    validPage = EE_FindValidPage(WRITE_IN_VALID_PAGE);

    /* Check if there is no valid page */
    if (validPage == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    /* Get the valid Page start Address */
    address = pagePointers[validPage];

    /* Get the valid Page end Address */
    pageEndAddress = address + HAL_EEPROMEMU_PAGE_SIZE/2 - 1;

    /* Check each active page address starting from begining */
    while (address < pageEndAddress) {
        /* Verify if Address and Address+2 contents are 0xFFFFFFFF */
        if ((*(__IO uint32_t*)address) == 0xFFFFFFFF) {
            /* Set variable data */
            flashStatus = HAL_FLASH_Program(TYPEPROGRAM_HALFWORD, (uint32_t)address, data);
            if (flashStatus != HAL_OK) {
                return flashStatus;
            }

            /* Set variable virtual address */
            return HAL_FLASH_Program(TYPEPROGRAM_HALFWORD, (uint32_t)++address, virtAddress);
        }
        else {
            /* Next address location */
            address += 2;
        }
    }

    /* Return PAGE_FULL in case the valid page is full */
    return HAL_EEPROMEMU_PAGE_FULL;
}

/**
  * @brief  Transfers last updated variables data from the full Page to
  *   an empty one.
  * @param  VirtAddress: 16 bit virtual address of the variable
  * @param  Data: 16 bit data to be written as variable value
  * @retval Success or error status:
  *           - FLASH_COMPLETE: on success
  *           - PAGE_FULL: if valid page is full
  *           - NO_VALID_PAGE: if no valid page was found
  *           - Flash error code: on write Flash error
  */
static uint16_t EE_TransferPageWriteVariable(uint8_t virtAddress, uint16_t data)
{
    HAL_StatusTypeDef flashStatus = HAL_OK;
    uint16_t *newPageAddress, *oldPageAddress;
    uint8_t validPage;
    uint16_t eepromStatus = 0;

    /* Get active Page for read operation */
    validPage = EE_FindValidPage(READ_FROM_VALID_PAGE);

    /* Page1 valid */
    if (validPage == page1) {
        /* New page address where variable will be moved to */
        newPageAddress = pagePointer0;

        /* Old page address where variable will be taken from */
        oldPageAddress = pagePointer1;
    }
    /* Page0 valid */
    else if (validPage == page0) {
        /* New page address where variable will be moved to */
        newPageAddress = pagePointer1;

        /* Old page address where variable will be taken from */
        oldPageAddress = pagePointer0;
    }
    else {
        return HAL_EEPROMEMU_NO_VALID_PAGE;       /* No valid Page */
    }

    /* Set the new Page status to RECEIVE_DATA status */
    flashStatus = HAL_FLASH_Program(TYPEPROGRAM_HALFWORD, (uint32_t)newPageAddress, PAGE_STATUS_RECEIVE_DATA);
    if (flashStatus != HAL_OK) {
        return flashStatus;
    }

    /* Write the variable passed as parameter in the new active page */
    eepromStatus = EE_VerifyPageFullWriteVariable(virtAddress, data);
    if (eepromStatus != HAL_OK) {
        return eepromStatus;
    }

    /* Transfer process: transfer variables from old to the new active page */
    return EE_TransferPage(oldPageAddress, newPageAddress);
}

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
