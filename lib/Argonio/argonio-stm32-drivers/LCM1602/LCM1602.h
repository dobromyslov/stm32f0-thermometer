/**
 * LCM1602.h
 *
 * I2C LCD driver.
 */

#ifndef LCM1602_H_
#define LCM1602_H_

#include "stm32f0xx.h"

/**
 * Commands.
 */
#define LCD_CLEAR_DISPLAY   0x01
#define LCD_RETURN_HOME     0x02
#define LCD_ENTRY_MODE_SET  0x04
#define LCD_DISPLAY_CONTROL 0x08
#define LCD_CURSOR_SHIFT    0x10
#define LCD_FUNCTION_SET    0x20
#define LCD_SET_CG_RAM_ADDR 0x40
#define LCD_SET_DD_RAM_ADDR 0x80
#define LCD_BACKLIGHT       0x08
#define LCD_NO_BACKLIGHT    0x00


/**
 *
 */
#define LCD_ENTRY_RIGHT             0x00
#define LCD_ENTRY_LEFT              0x02
#define LCD_ENTRY_SHIFT_INCREMENT   0x01
#define LCD_ENTRY_SHIFT_DECREMENT   0x00

/**
 *
 */
#define LCD_DISPLAY_ON  0x04
#define LCD_DISPLAY_OFF 0x00
#define LCD_CURSOR_ON   0x02
#define LCD_CURSOR_OFF  0x00
#define LCD_BLINK_ON    0x01
#define LCD_BLINK_OFF   0x00

/**
 *
 */
#define LCD_8BIT_MODE   0x10
#define LCD_4BIT_MODE   0x00
#define LCD_2LINE       0x08
#define LCD_1LINE       0x00
#define LCD_5x10DOTS    0x04
#define LCD_5x8DOTS     0x00

#define LCD_EN 0x04 // Enable bit
#define LCD_RW 0x02 // Read/Write bit
#define LCD_RS 0x01 // Register select bit

void LCD_initialize(I2C_HandleTypeDef *hi2c);
void LCD_refresh(I2C_HandleTypeDef *hi2c);

void LCD_clear(I2C_HandleTypeDef *hi2c);
void LCD_setBlink(I2C_HandleTypeDef *hi2c, uint8_t enable);
void LCD_home(I2C_HandleTypeDef *hi2c);
void LCD_locate(I2C_HandleTypeDef *hi2c, int row, int col);
void LCD_create_char(I2C_HandleTypeDef *hi2c, uint8_t number, uint8_t *bitmap);
void LCD_print(I2C_HandleTypeDef *hi2c, const uint8_t *text);

void LCD_buffer_print(int row, int col, uint8_t *str);
void LCD_buffer_clear();

#endif /* LCM1602_H_ */
