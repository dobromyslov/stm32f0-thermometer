/**
 * LCM1602.c
 */

#include "LCM1602.h"
#include <string.h>
#include "argonio_stm32_drivers_conf.h"


uint8_t buffer_front[LCD_ROWS][LCD_COLS];
uint8_t buffer_back[LCD_ROWS][LCD_COLS];
uint8_t blink = 0;

void expanderWrite(I2C_HandleTypeDef *hi2c, uint8_t value) {
    uint8_t buffer = value | LCD_BACKLIGHT;

    uint8_t result = HAL_I2C_Master_Transmit(hi2c, LCD_I2C_ADDRESS, &buffer, 1, 1000);
    if (result != HAL_OK) {
        buffer = 1;
    }
}

void pulseEnable(I2C_HandleTypeDef *hi2c, uint8_t value) {
    expanderWrite(hi2c, value | LCD_EN);
    expanderWrite(hi2c, value & ~LCD_EN);
}

void write4bits(I2C_HandleTypeDef *hi2c, uint8_t value) {
    expanderWrite(hi2c, value);
    pulseEnable(hi2c, value);
}

void send(I2C_HandleTypeDef *hi2c, uint8_t value, uint8_t mode) {
    uint8_t h = value & 0xf0;
    uint8_t l = (value << 4) & 0xf0;
    write4bits(hi2c, h | mode);
    write4bits(hi2c, l | mode);
}

void command(I2C_HandleTypeDef *hi2c, uint8_t value) {
    send(hi2c, value, 0);
}

void LCD_clear(I2C_HandleTypeDef *hi2c) {
    uint8_t i;
    for (i = 0; i < LCD_ROWS; i++) {
        memset(buffer_front[i], ' ', LCD_COLS);
        memset(buffer_back[i], ' ', LCD_COLS);
    }

    command(hi2c, LCD_CLEAR_DISPLAY);
}

void LCD_setBlink(I2C_HandleTypeDef *hi2c, uint8_t enable) {
    if (blink != enable) {
        blink = enable;
        command(hi2c, LCD_DISPLAY_CONTROL | LCD_DISPLAY_ON | LCD_CURSOR_OFF | (blink ? LCD_BLINK_ON : LCD_BLINK_OFF));
    }
}

void LCD_home(I2C_HandleTypeDef *hi2c) {
    command(hi2c, LCD_RETURN_HOME);
}

void LCD_locate(I2C_HandleTypeDef *hi2c, int row, int col) {
    static int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
    command(hi2c, LCD_SET_DD_RAM_ADDR | ((col % LCD_COLS) + row_offsets[row % LCD_ROWS]));
}

void LCD_create_char(I2C_HandleTypeDef *hi2c, uint8_t number, uint8_t *bitmap) {
    number &= 0x7; // we only have 8 locations 0-7
    command(hi2c, LCD_SET_CG_RAM_ADDR | (number << 3));

    uint8_t i;
    for (i = 0; i < 7; i++) {
        send(hi2c, bitmap[i], LCD_RS);
    }
    send(hi2c, 0, LCD_RS);
}

void LCD_print(I2C_HandleTypeDef *hi2c, const uint8_t *text) {
    int i = 0;
    int tlen = strlen(text);
    for (i = 0; i < tlen; i++) {
        send(hi2c, text[i], LCD_RS);
    }
}

void LCD_initialize(I2C_HandleTypeDef *hi2c) {
    expanderWrite(hi2c, LCD_BACKLIGHT);

    write4bits(hi2c, 0x03 << 4);
    write4bits(hi2c, 0x30);
    write4bits(hi2c, 0x30);

    write4bits(hi2c, 0x20);

    command(hi2c, LCD_FUNCTION_SET | LCD_2LINE);
    command(hi2c, LCD_DISPLAY_CONTROL | LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINK_OFF);
    LCD_clear(hi2c);

    command(hi2c, LCD_ENTRY_MODE_SET | LCD_ENTRY_LEFT | LCD_ENTRY_SHIFT_DECREMENT);
}

void LCD_buffer_clear() {
    // Fill back buffer with spaces
    uint8_t i;
    for (i = 0; i < LCD_ROWS; i++) {
        memset(buffer_back[i], ' ', LCD_COLS);
    }
}

void LCD_buffer_print(int row, int col, uint8_t *str) {
    if (row < LCD_ROWS && col < LCD_COLS) {
        uint8_t i, c;
        uint8_t *brow = &buffer_back[row][col];
        for (i = 0, c = *str; i < LCD_COLS && c; c = *++str) {
            *brow++ = c;
        }
    }
}

void LCD_refresh(I2C_HandleTypeDef *hi2c) {
    uint8_t changed;
    uint8_t i, j;
    uint8_t *frow, *brow;

    for (i = 0; i < LCD_ROWS; i++) {
        changed = 0;
        frow = buffer_front[i];
        brow = buffer_back[i];
        for (j = 0; j < LCD_COLS; j++) {
            if (*frow != *brow) {
                *frow = *brow;
                changed = 1;
            }
            frow++;
            brow++;
        }

        if (changed) {
            // Update row
            LCD_locate(hi2c, i, 0);
            frow = buffer_front[i];
            for (j = 0; j < LCD_COLS; j++) {
                send(hi2c, *frow++, LCD_RS);
            }
        }
    }
}
