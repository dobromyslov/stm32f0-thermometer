/**
 * Key definitions for matrix keyboard 4x4.
 */

#ifndef LIB_ARGONIO_ARGONIO_STM32_DRIVERS_MATRIXKEYBOARD_MATRIXKEYBOARD_4X4_TYPE1_H_
#define LIB_ARGONIO_ARGONIO_STM32_DRIVERS_MATRIXKEYBOARD_MATRIXKEYBOARD_4X4_TYPE1_H_

#include "MatrixKeyboard.h"

extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_1;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_2;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_3;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_F1;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_4;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_5;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_6;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_F2;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_7;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_8;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_9;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_BACK;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_ESC;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_0;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_TAB;
extern const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_DO;

/**
 * Initialization struct.
 *
MatrixKeyboard_Device_t mainKeyboard = {
        .rows = 4,
        .cols = 4,
        .scanPins = (MatrixKeyboard_Pin_t[]){
                {.port = GPIOA, .pin = GPIO_PIN_7, .pinNumber = 7},
                {.port = GPIOA, .pin = GPIO_PIN_6, .pinNumber = 6},
                {.port = GPIOA, .pin = GPIO_PIN_5, .pinNumber = 5},
                {.port = GPIOA, .pin = GPIO_PIN_4, .pinNumber = 4}
        },
        .readPins = (MatrixKeyboard_Pin_t[]){
                {.port = GPIOA, .pin = GPIO_PIN_3, .pinNumber = 3},
                {.port = GPIOA, .pin = GPIO_PIN_2, .pinNumber = 2},
                {.port = GPIOA, .pin = GPIO_PIN_1, .pinNumber = 1},
                {.port = GPIOA, .pin = GPIO_PIN_0, .pinNumber = 0}
        },
        .keyboardBuffer = (uint8_t[]){0, 0, 0, 0},
        .keyboardEvents = (MatrixKeyboard_KeyEvent_t[]){
            {0,0,0,0},
            {0,0,0,0},
            {0,0,0,0},
            {0,0,0,0}
        },
        .pressedKeysCount = 0
};
*/

#endif /* LIB_ARGONIO_ARGONIO_STM32_DRIVERS_MATRIXKEYBOARD_MATRIXKEYBOARD_4X4_TYPE1_H_ */
