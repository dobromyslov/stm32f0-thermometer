/*
 * MatrixKeyboard.h
 *
 * Example:
 * for (;;) {
 *     if (MatrixKeyboard_scan()) {
 *         vTaskDelay(20);
 *         MatrixKeyboard_scan();
 *     }
 *     MatrixKeyboard_updateEvents();
 *     if (MatrixKeyboard_getKeyEvent(&MATRIX_KEYBOARD_4X4_T1_KEY_F1) == MATRIX_KEYBOARD_KEY_DOWN) {
 *         // do something
 *     }
 * }
 */

#ifndef LIB_ARGONIO_ARGONIO_STM32_DRIVERS_MATRIXKEYBOARD_MATRIXKEYBOARD_H_
#define LIB_ARGONIO_ARGONIO_STM32_DRIVERS_MATRIXKEYBOARD_MATRIXKEYBOARD_H_

#include "argonio_stm32_drivers_conf.h"

typedef struct {
    GPIO_TypeDef *port;
    uint16_t     pin;
    uint8_t      pinNumber;
} MatrixKeyboard_Pin_t;

typedef struct {
    uint8_t row;
    uint8_t col;
} MatrixKeyboard_Key_t;

typedef enum {
    MATRIX_KEYBOARD_KEY_DOWN  = 1,
    MATRIX_KEYBOARD_KEY_PRESS = 2,
    MATRIX_KEYBOARD_KEY_UP    = 3
} MatrixKeyboard_KeyEvent_t;

typedef struct {
    /**
     * Keyboard rows count.
     * Can not contain more than 8 rows (uint8_t restriction).
     */
    uint8_t rows;

    /**
     * Keyboard buttons count in each row.
     * Can not contain more than 8 buttons (uint8_t restriction).
     */
    uint8_t cols;

    /**
     * Array of MCU pins to be scanned with "1" - rows.
     */
    MatrixKeyboard_Pin_t *scanPins;

    /**
     * Array of MCU pins to be read during scan - cols.
     */
    MatrixKeyboard_Pin_t *readPins;

    /**
     * Runtime keyboard state.
     */

    /**
     * Buffer to store keys status.
     * Contain one byte per row, i,e, up to 8 keys per row.
     *
     * Size of array = rows count.
     */
    uint8_t *keyboardBuffer;

    /**
     * Keyboard events matrix.
     * Contains current events for each button in matrix.
     * Can not contain more than 8 keys per row.
     *
     * Size of matrix is [rows][cols]
     */
    MatrixKeyboard_KeyEvent_t *keyboardEvents;

    /**
     * Current simultaneously pressed keys count.
     */
    uint8_t pressedKeysCount;

} MatrixKeyboard_Device_t;

void MatrixKeyboard_init(MatrixKeyboard_Device_t *device);
uint8_t MatrixKeyboard_scan(MatrixKeyboard_Device_t *device);
void MatrixKeyboard_updateEvents(MatrixKeyboard_Device_t *device);

uint8_t MatrixKeyboard_getPressedKeysCount(MatrixKeyboard_Device_t *device);
uint8_t MatrixKeyboard_getRowStatus(MatrixKeyboard_Device_t *device, uint8_t row);
uint8_t MatrixKeyboard_getStatus(MatrixKeyboard_Device_t *device, uint8_t row, uint8_t col);
uint8_t MatrixKeyboard_getKeyStatus(MatrixKeyboard_Device_t *device, MatrixKeyboard_Key_t *key);
MatrixKeyboard_KeyEvent_t MatrixKeyboard_getKeyEvent(MatrixKeyboard_Device_t *device, MatrixKeyboard_Key_t *key);

#endif /* LIB_ARGONIO_ARGONIO_STM32_DRIVERS_MATRIXKEYBOARD_MATRIXKEYBOARD_H_ */
