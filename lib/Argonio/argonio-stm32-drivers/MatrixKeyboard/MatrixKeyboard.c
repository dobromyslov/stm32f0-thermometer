#include "MatrixKeyboard.h"
#include <string.h>

/**
 * Sets pin state to low or to high.
 *
 * @param state 1 = HIGH
 *              0 = LOW
 */
__attribute__( ( always_inline ) ) __STATIC_INLINE void __setPinState(MatrixKeyboard_Pin_t *kbPin, uint8_t state) {
    if (state) {
        kbPin->port->BSRRL = kbPin->pin;
    }
    else {
        kbPin->port->BSRRH = kbPin->pin;
    }
}

/**
 * Returns current pin state.
 *
 * @returns 1 = HIGH
 *          0 = LOW
 *
 */
__attribute__( ( always_inline ) ) __STATIC_INLINE uint8_t __getPinState(MatrixKeyboard_Pin_t *kbPin) {
    return (kbPin->port->IDR & kbPin->pin) != 0;
}

/**
 * Initializes matrix keyboard.
 * Sets SCAN-pins and READ-pins.
 */
void MatrixKeyboard_init(MatrixKeyboard_Device_t *device) {
    uint8_t i;
    GPIO_InitTypeDef GPIO_InitStruct;

    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;

    // Initialize SCAN-pins with Pull Up - rows
    for (i = 0; i < device->rows; i++) {
        GPIO_InitStruct.Pin  = device->scanPins[i].pin;
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        HAL_GPIO_Init(device->scanPins[i].port, &GPIO_InitStruct);

        __setPinState(&device->scanPins[i], 0);
    }

    // Initialize READ-pins with PullDown - columns
    for (i = 0; i < device->cols; i++) {
        GPIO_InitStruct.Pin  = device->readPins[i].pin;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_PULLDOWN;
        HAL_GPIO_Init(device->readPins[i].port, &GPIO_InitStruct);
    }

    memset(device->keyboardEvents, 0, device->rows * device->cols * sizeof(*device->keyboardEvents));
}

/**
 * Sequentially sets each SCAN-pin to HIGH and reads each READ-pin state
 * to detect pressed button in the row.
 *
 * To eliminate contact bounce:
 * if (MatrixKeyboard_scan()) {
 *     vTaskDelay(20);
 *     MatrixKeyboard_scan();
 * }
 * MatrixKeyboard_updateEvents();
 *
 * @returns TRUE if keyboard state changed.
 */
uint8_t MatrixKeyboard_scan(MatrixKeyboard_Device_t *device) {
    uint8_t i, j, rowState, keyState, changed = 0;

    device->pressedKeysCount = 0;
    for (i = 0; i < device->rows; i++) {
        __setPinState(&device->scanPins[i], 1);
        rowState = 0;
        for (j = 0; j < device->cols; j++) {
            if ((keyState = __getPinState(&device->readPins[j]))) {
                rowState |=  keyState << j;
                device->pressedKeysCount++;
            }
        }
        if (device->keyboardBuffer[i] != rowState) {
            device->keyboardBuffer[i] = rowState;
            changed = 1;
        }
        __setPinState(&device->scanPins[i], 0);
    }

    return changed;
}

/**
 * Updates events for each button.
 */
void MatrixKeyboard_updateEvents(MatrixKeyboard_Device_t *device) {
    uint8_t i, j, status;
    MatrixKeyboard_KeyEvent_t *event;

    for (i = 0; i < device->rows; i++) {
        for (j = 0; j < device->cols; j++) {
            status = MatrixKeyboard_getStatus(device, i, j);
            event = &device->keyboardEvents[i * device->cols + j];

            // Check previous event and set new event according to button status
            switch (*event) {
                case MATRIX_KEYBOARD_KEY_DOWN:
                    *event = status ? MATRIX_KEYBOARD_KEY_PRESS : MATRIX_KEYBOARD_KEY_UP;
                    break;

                case MATRIX_KEYBOARD_KEY_PRESS:
                    if (!status) {
                        *event = MATRIX_KEYBOARD_KEY_UP;
                    }
                    break;

                case MATRIX_KEYBOARD_KEY_UP:
                    *event = status ? MATRIX_KEYBOARD_KEY_DOWN : 0;
                    break;

                default:
                    if (status) {
                        *event = MATRIX_KEYBOARD_KEY_DOWN;
                    }
            }
        }
    }
}

/**
 * Returns currently pressed keys count.
 */
uint8_t MatrixKeyboard_getPressedKeysCount(MatrixKeyboard_Device_t *device) {
    return device->pressedKeysCount;
}

/**
 * Returns row status as uint8_t.
 */
uint8_t MatrixKeyboard_getRowStatus(MatrixKeyboard_Device_t *device, uint8_t row) {
    return device->keyboardBuffer[row];
}

/**
 * Returns status of the specified key by row and column.
 */
uint8_t MatrixKeyboard_getStatus(MatrixKeyboard_Device_t *device, uint8_t row, uint8_t col) {
    return (device->keyboardBuffer[row] & (1 << col)) > 0;
}

/**
 * Returns status of the specified key by its definition.
 */
uint8_t MatrixKeyboard_getKeyStatus(MatrixKeyboard_Device_t *device, MatrixKeyboard_Key_t *key) {
    return (device->keyboardBuffer[key->row] & (1 << key->col)) > 0;
}

/**
 * Returns current event of the specified key by its definition.
 */
MatrixKeyboard_KeyEvent_t MatrixKeyboard_getKeyEvent(MatrixKeyboard_Device_t *device, MatrixKeyboard_Key_t *key) {
    return device->keyboardEvents[key->row * device->cols + key->col];
}
