#include "MatrixKeyboard_4x4_Type1.h"

const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_1    = {.row = 0, .col = 0};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_2    = {0, 1};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_3    = {0, 2};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_F1   = {0, 3};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_4    = {1, 0};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_5    = {1, 1};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_6    = {1, 2};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_F2   = {1, 3};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_7    = {2, 0};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_8    = {2, 1};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_9    = {2, 2};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_BACK = {2, 3};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_ESC  = {3, 0};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_0    = {3, 1};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_TAB  = {3, 2};
const MatrixKeyboard_Key_t MATRIX_KEYBOARD_4X4_T1_KEY_DO   = {3, 3};
