/**
 * OneWire.h
 *
 * Theory of operation
 * ===================
 * A 1-Wire bus uses only one wire for signaling and power. Communication is asynchronous and
 * half-duplex, and it follows a strict master-slave scheme. One or several slave devices can be
 * connected to the bus at the same time. Only one master should be connected to the bus.
 *
 * The bus is idle high, so there must be a pull-up resistor present. To determine the value of the
 * pull-up resistor, see the data sheet of the slave device(s). All devices connected to the bus
 * must be able to drive the bus low. A open-collector or open-drain buffer is required if a device
 * is connected through a pin that can not be put in a tri-state mode.
 *
 * Signaling on the 1-Wire bus is divided into time slots of 60us. One data bit is transmitted on
 * the bus per time slot. Slave devices are allowed to have a time base that differs significantly
 * from the nominal time base. This however, requires the timing of the master to be very precise,
 * to ensure correct communication with slaves with different time bases.
 *
 * *****************
 * Basic bus signals
 * *****************
 * The master initiates every communication on the bus down to the bit-level. This means that for
 * every bit that is to be transmitted, regardless of direction, the master has to initiate the bit
 * transmission. This is always done by pulling the bus low, which will synchronize the timing logic
 * of all units. There are 5 basic commands for communication on the 1-Wire bus:
 * - “Write 1”
 * - “Write 0”
 * - “Read”
 * - “Reset”
 * - “Presence”
 *
 * “Write 1” signal
 * ================
 * A “Write 1” signal is shown in Figure 1. The master pulls the bus low for 1 to 15 μs. It then
 * releases the bus for the rest of the time slot.
 * ___     __________________
 *    \___/
 *
 *
 * “Write 0” signal
 * ================
 * A “Write 0” signal is shown in Figure 2. The master pulls the bus low for a period of
 * at least 60 μs, with a maximum length of 120 μs.
 *___                  ______
 *   \________________/
 *
 * “Read” signal
 * =============
 * A “Read” signal is shown in Figure 3. The master pulls the bus low for 1 to 15 μs. The slave
 * then holds the bus low if it wants to send a ‘0’. If it wants to send a ‘1’, it simply releases
 * the line. The bus should be sampled 15μs after the bus was pulled low. As seen from the master’s
 * side, the “Read” signal is in essence a “Write 1” signal. It is the internal state of the slave,
 * rather than the signal itself that dictates whether it is a “Write 1” or “Read” signal.
 *___     ___________________
 *   \___/____________/
 *
 * “Reset/Presence” signal
 * =======================
 * A “Reset” and “Presence” signal is shown in Figure 4. Note that the time scale is different from
 * the first waveforms. The master pulls the bus low for at least 8 time slots, or 480μs and then
 * releases it. This long low period is called the “Reset” signal. If there is a slave present, it
 * should then pull the bus low within 60μs after it was released by the master and hold it low for
 * at least 60μs. This response is called a “Presence” signal. If no presence signal is issued on
 * the bus, the master must assume that no device is present on the bus, and further communication
 * is not possible.
 *___              ______     _____
 *   \____________/      \___/
 *      RESET           PRESENCE
 *
 * Generating the 1-Wire signals in software only is straightforward. Simply changing the direction
 * and value of a general purpose I/O pin and generating the required delay is sufficient.
 *
 * *********************
 * ROM function commands
 * *********************
 * Every 1-Wire device contains a globally unique 64 bit identifier number stored in ROM. This
 * number can be used to facilitate addressing or identification of individual devices on the bus.
 * The identifier consists of three parts: an 8 bit family code, a 48 bit serial number and an 8 bit
 * CRC computed from the first 56 bits. A small set of commands that operate on the 64 bit
 * identifier are defined. These are called ROM function commands. Table 2 lists the six defined
 * ROM commands.
 *
 * Command              Code    Usage
 * =========            ====    ==============
 * READ ROM             33H     Identification
 * SKIP ROM             CCH     Skip addressing
 * MATCH ROM            55H     Address specific device
 * SEARCH ROM           F0H     Obtain IDs of all devices on the bus
 * OVERDRIVE SKIP ROM   3CH     Overdrive version of SKIP ROM
 * OVERDRIVE MATCH ROM  69H     Overdrive version of MATCH ROM
 *
 * READ ROM command
 * ================
 * The “READ ROM” command can be used on a bus with a single slave to read the 64 bit unique
 * identifier. If there are several slave devices connected to the bus, the result of this command
 * will be the AND result of all slave device identifiers. Assumed that communication is flawless,
 * the presence of several slaves is indicated by a failed CRC.
 *
 * SKIP ROM command
 * ================
 * The “SKIP ROM” command can be used when no specific slave is targeted. On a one-slave bus, the
 * “SKIP ROM” command is sufficient for addressing. On a multiple-slave bus, the “SKIP ROM” command
 * can be used to address all devices at once. This is only useful when sending commands to slave
 * devices, e.g. to start temperature conversions on several temperature sensors at once. It is not
 * possible to use the “SKIP ROM” command when reading from slave devices on a multiple-slave bus.
 *
 * MATCH ROM command
 * =================
 * The “MATCH ROM” command is used to address individual slave devices on the bus. After the
 * “MATCH ROM” command, the complete 64 bit identifier is transmitted on the bus When this is done,
 * only the device with exactly this identifier is allowed to answer until the next reset pulse is
 * received.
 *
 * SEARCH ROM command
 * ==================
 * The “SEARCH ROM” command can be used when the identifiers of all slave devices are not known in
 * advance. It makes it possible to discover the identifiers of all slaves present on the bus. First
 * the “SEARCH ROM” command is transmitted on the bus. The master then reads one bit from the bus.
 * Each slave places the first bit of its identifier on the bus. The master will read this as the
 * logical AND result of the first bit of all slave identifiers. The master then reads one more bit
 * from the bus. Each slave then places the complement of the first bit of its identifier on the
 * bus. The master will read this as the logical AND of the complement of the first bit of the
 * identifier of all slaves. If all devices have 1 as the first bit, the master will have read 10b.
 * Similarly if all devices have 0 as the first bit, the master will have read 01b. In these cases,
 * the bit can be stored as the first bit of all addresses. The master will then write back this
 * bit, which in effect will tell all slaves to keep sending identifier bits. If there are devices
 * with both 0 and 1 as the first bit in the identifier on the bus, the master will have read 00.
 * In this case the master must make a choice, whether to continue with the addresses that have 0
 * in this position or 1. The choice is transmitted on the bus, in effect making all slaves that do
 * not have this bit in this position of the identifier enter an idle state.
 *
 * The master then goes on to read the next bit, and the process is repeated until all 64 bits are
 * read. The master should then have discovered one complete 64 bit identifier. To discover more
 * identifiers the “SEARCH ROM” command should be run again, but this time a different choice for
 * the bit value should be made the first time there is a discrepancy. Repeating this once for each
 * slave device should discover all slaves. Note that when one search has been performed, all
 * slaves except of one should have entered an idle state. It is now possible to communicate with
 * the active slave without specifically addressing it with the MATCH ROM command.
 *
 * Overdrive ROM commands
 * ======================
 * The overdrive ROM commands are not covered here, since overdrive mode is outside the scope of
 * this document, only covering standard speed.
 *
 * ************************
 * Memory/function commands
 * ************************
 * Memory/function commands are commands that are specific to one device, or a class of devices.
 * These commands typically deal with reading and writing of internal memory and registers in slave
 * devices. A number of memory/function commands are defined, but all commands are not used by all
 * devices. The order of writes and reads is specific to each device, not part of the general
 * specification. Memory commands will therefore not be covered in detail here.
 *
 * ***********************
 * Putting it all together
 * ***********************
 * All 1-Wire devices follow a basic communication sequence:
 *  1. The master sends the “Reset” pulse.
 *  2. The slave(s) respond with a ”Presence” pulse.
 *  3. The master sends a ROM command. This effectively addresses one or several slave devices.
 *  4. The master sends a Memory command.
 *
 * Note that to reach each step, the last step has to be completed. It is however not necessary to
 * complete the whole sequence. E.g. it is possible to send a new “Reset” after finishing a ROM
 * command to start a new communication.
 */

#ifndef ONEWIRE_H_
#define ONEWIRE_H_

#include <stdint.h>
#include "stm32f0xx.h"

#define ONE_WIRE_READ_ROM               0x33
#define ONE_WIRE_SKIP_ROM               0xCC
#define ONE_WIRE_MATCH_ROM              0x55
#define ONE_WIRE_SEARCH_ROM             0xF0

#define ONE_WIRE_OVERDRIVE_SKIP_ROM     0x3C
#define ONE_WIRE_OVERDRIVE_MATCH_ROM    0x69

/**
 * Return values for OneWire_SearchRom.
 */
typedef enum {
    ONE_WIRE_ROM_SEARCH_FINISHED = 0,
    ONE_WIRE_ROM_SEARCH_CONTINUE,
    ONE_WIRE_ROM_SEARCH_FAILED
} OneWire_RomSearchResult_t;

/**
 * Values for OneWire_ScanResult_t.error.
 */
typedef enum {
    /**
     * No error occurred.
     * Scan completed successfully.
     */
    ONE_WIRE_SCAN_OK = 0,

    /**
     * Bus did not return presence signal.
     * Maybe there is no devices connected.
     */
    ONE_WIRE_SCAN_ERROR_NO_PRESENCE,

    /**
     * See docs for OneWire_SearchRom.
     */
    ONE_WIRE_SCAN_ERROR_ROM_SEARCH_FAILED,

    /**
     * One or more found ROM has invalid CRC.
     */
    ONE_WIRE_SCAN_ERROR_INVALID_ROM_CRC,

    /**
     * Passed zero devicesLimit parameter.
     * There is no place to store found devices in.
     */
    ONE_WIRE_SCAN_ERROR_ZERO_DEVICES_LIMIT

} OneWire_ScanEror_t;

/**
 * One Wire bus definition.
 */
typedef struct {
    GPIO_TypeDef * port;
    uint16_t pin;
    uint8_t pinNumber;

#ifdef ONE_WIRE_USE_STRONG_PULL_UP
    // Strong Pull-Up
    uint8_t useStrongPullUp;
    GPIO_TypeDef * spuPort;
    uint16_t spuPin;
#endif
} OneWire_Bus_t;

/**
 * One Wire device definition.
 */
typedef struct {
    uint8_t rom[8];
    uint8_t crcValid;
    OneWire_Bus_t *bus;
} OneWire_Device_t;

/**
 * OneWire search result definition.
 */
typedef struct {
    OneWire_Device_t *devices;
    uint8_t count;
    OneWire_ScanEror_t error;
} OneWire_ScanResult_t;

/**
 * 1-Wire low level functions.
 */
void OneWire_WriteByte(OneWire_Bus_t *bus, uint8_t data);
uint8_t OneWire_ReadByte(OneWire_Bus_t *bus);

/*******************************************************************************
 * 1-Wire bus control commands.
 ******************************************************************************/
/**
 * Initializes 1-Wire bus.
 *
 * Example of bus definition:
 *
 * OneWire_Bus_t oneWireBus = {
 *      .port      = GPIOA,
 *      .pin       = GPIO_PIN_15,
 *      .pinNumber = 15
 * };
 */
void OneWire_Init(OneWire_Bus_t *bus);

uint8_t OneWire_ResetAndDetectPresence(OneWire_Bus_t *bus);

void OneWire_ReadRom(OneWire_Bus_t *bus, uint8_t resultRom[8]);

void OneWire_SkipRom(OneWire_Bus_t *bus);

void OneWire_MatchRom(OneWire_Bus_t *bus, uint8_t rom[8]);

OneWire_RomSearchResult_t OneWire_SearchRom(OneWire_Bus_t *bus, uint8_t *bitPattern, uint8_t *lastDeviationBit);

OneWire_ScanResult_t OneWire_ScanBus(OneWire_Bus_t *bus, OneWire_Device_t *resultDevices, uint8_t devicesLimit);

/**
 * @todo Not implemented yet.
 */
//OneWire_ScanResult_t OneWire_ScanFamily(uint8_t familyCode, uint8_t limit, OneWire_Device_t *result);

/**
 * CRC validation functions.
 */
uint8_t OneWire_CRC8(uint8_t data, uint8_t seed);
uint8_t OneWire_ValidateRomCRC(uint8_t *rom);

/**
 * ROM hex conversion.
 */
void OneWire_hex2rom(const char romHex[16], uint8_t rom[8]);
void OneWire_rom2hex(const uint8_t rom[8], char romHex[16]);

#endif /* ONEWIRE_H_ */
