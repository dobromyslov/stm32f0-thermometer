/**
 * OneWire.c
 *
 * 1-Wire protocol implementation.
 * See docs in OneWire.h
 */

#include "OneWire.h"
#include <string.h>
#include "Delay.h"
#include "argonio_stm32_drivers_conf.h"

#define GPIO_MODE             ((uint32_t)0x00000003)
#define GPIO_OUTPUT_TYPE      ((uint32_t)0x00000010)

/**
 * Enter critical section.
 */
__attribute__( ( always_inline ) ) __STATIC_INLINE void __enter_critical_section(void) {
    __disable_irq();
    __DSB();
    __ISB();
}

/**
 * Exit critical section.
 */
__attribute__( ( always_inline ) ) __STATIC_INLINE void __exit_critical_section(void) {
    __enable_irq();
}

#ifdef ONE_WIRE_USE_STRONG_PULL_UP
__attribute__( ( always_inline ) ) __STATIC_INLINE void OneWire_EnableSPU(OneWire_Bus_t *bus) {
    HAL_GPIO_WritePin(bus->spuPort, bus->spuPin, 1);
}

__attribute__( ( always_inline ) ) __STATIC_INLINE void OneWire_DisableSPU(OneWire_Bus_t *bus) {
    HAL_GPIO_WritePin(bus->spuPort, bus->spuPin, 0);
}
#endif

/**
 * 1-Wire bus initialization method.
 *
 * Must be executed prior to any other 1-Wire functions.
 */
void OneWire_Init(OneWire_Bus_t *bus) {

    #ifdef ONE_WIRE_USE_STRONG_PULL_UP
    // Strong pull-up
    if (bus->useStrongPullUp) {
        GPIO_InitTypeDef spuGpioInitStruct = {
            bus->spuPin,
            GPIO_MODE_OUTPUT_OD,
            GPIO_NOPULL,
            GPIO_SPEED_LOW,
            0
        };
        HAL_GPIO_Init(bus->spuPort, &spuGpioInitStruct);
    }
    #endif

    uint32_t temp;

    /* Configure IO Direction mode (Input, Output, Alternate or Analog) */
    temp = bus->port->MODER;
    temp &= ~(GPIO_MODER_MODER0 << (bus->pinNumber * 2));
    temp |= ((GPIO_MODE_OUTPUT_OD & GPIO_MODE) << (bus->pinNumber * 2));
    bus->port->MODER = temp;

    /* Configure the IO Speed */
    temp = bus->port->OSPEEDR;
    temp &= ~(GPIO_OSPEEDER_OSPEEDR0 << (bus->pinNumber * 2));
    temp |= (GPIO_SPEED_HIGH << (bus->pinNumber * 2));
    bus->port->OSPEEDR = temp;

    /* Configure the IO Output Type */
    temp = bus->port->OTYPER;
    temp &= ~(GPIO_OTYPER_OT_0 << bus->pinNumber) ;
    temp |= (((GPIO_MODE_OUTPUT_OD & GPIO_OUTPUT_TYPE) >> 4) << bus->pinNumber);
    bus->port->OTYPER = temp;

    /* Activate the Pull-up or Pull down resistor for the current IO */
    temp = bus->port->PUPDR;
    temp &= ~(GPIO_PUPDR_PUPDR0 << (bus->pinNumber * 2));
    #ifdef ONE_WIRE_USE_INTERNAL_PULL_UP
        temp |= (GPIO_PULLUP << (bus->pinNumber * 2));
    #else
        temp |= (GPIO_NOPULL << (bus->pinNumber * 2));
    #endif
    bus->port->PUPDR = temp;
}

/**
 * Sets data bus to LOW state or releases the bus to HIGH.
 *
 * @param state 1 = Input  mode (Release bus)
 *              0 = Output mode in open-drain (as stated in the specification)
 */
void OneWire_SetBusState(OneWire_Bus_t *bus, uint8_t state) {
    uint32_t temp = 0;

    if (state) {
        //GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        //HAL_GPIO_Init(ONE_WIRE_GPIO, &GPIO_InitStruct);

        temp = bus->port->MODER;
        temp &= ~(GPIO_MODER_MODER0 << (bus->pinNumber * 2));
        temp |= ((GPIO_MODE_INPUT & GPIO_MODE) << (bus->pinNumber * 2));
        bus->port->MODER = temp;

        bus->port->BSRRL = bus->pin;
    }
    else {
        //GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
        //HAL_GPIO_Init(ONE_WIRE_GPIO, &GPIO_InitStruct);

        temp = bus->port->MODER;
        temp &= ~(GPIO_MODER_MODER0 << (bus->pinNumber * 2));
        temp |= ((GPIO_MODE_OUTPUT_OD & GPIO_MODE) << (bus->pinNumber * 2));
        bus->port->MODER = temp;

        /*temp = ONE_WIRE_GPIO->OSPEEDR;
        temp &= ~(GPIO_OSPEEDER_OSPEEDR0 << (ONE_WIRE_DATA_PIN_NUMBER * 2));
        temp |= (GPIO_SPEED_HIGH << (ONE_WIRE_DATA_PIN_NUMBER * 2));
        ONE_WIRE_GPIO->OSPEEDR = temp;

        temp = ONE_WIRE_GPIO->OTYPER;
        temp &= ~(GPIO_OTYPER_OT_0 << ONE_WIRE_DATA_PIN_NUMBER) ;
        temp |= (((GPIO_MODE_OUTPUT_OD & GPIO_OUTPUT_TYPE) >> 4) << ONE_WIRE_DATA_PIN_NUMBER);
        ONE_WIRE_GPIO->OTYPER = temp;*/

        bus->port->BSRRH = bus->pin;
    }
    /*
    GPIO_InitStruct.Mode = state ? GPIO_MODE_INPUT : GPIO_MODE_OUTPUT_OD;
    HAL_GPIO_Init(ONE_WIRE_GPIO, &GPIO_InitStruct);
    HAL_GPIO_WritePin(ONE_WIRE_GPIO, ONE_WIRE_DATA_PIN, state);
    */
}

uint8_t OneWire_ReadBusState(OneWire_Bus_t *bus) {
    //return HAL_GPIO_ReadPin(bus->port, bus->pin);
    return (bus->port->IDR & bus->pin) != 0;
}

uint8_t OneWire_ReadBit(OneWire_Bus_t *bus) {
    uint8_t bit;
    __enter_critical_section();

    OneWire_SetBusState(bus, 0);
    CriticalSection_Delay(6);
    OneWire_SetBusState(bus, 1);
    CriticalSection_Delay(9);
    bit = OneWire_ReadBusState(bus);
    CriticalSection_Delay(55);

    __exit_critical_section();
    return bit;
}

void OneWire_WriteBit(OneWire_Bus_t *bus, uint8_t bit) {
    __enter_critical_section();

    OneWire_SetBusState(bus, 0);
    if (bit) {
        CriticalSection_Delay(6);
        OneWire_SetBusState(bus, 1);
        CriticalSection_Delay(64);
    }
    else {
        CriticalSection_Delay(60);
        OneWire_SetBusState(bus, 1);
        CriticalSection_Delay(10);
    }

    __exit_critical_section();
}

/**
 * Reset the 1-Wire bus and detect slave presence on it.
 *
 * @return 0 - no one slave found on the bus
 *         1 - any one slave is connected to the bus
 */
uint8_t OneWire_ResetAndDetectPresence(OneWire_Bus_t *bus) {
    uint8_t bit;
    __enter_critical_section();

    OneWire_SetBusState(bus, 0);
    CriticalSection_Delay(485);
    OneWire_SetBusState(bus, 1);
    CriticalSection_Delay(70);
    bit = OneWire_ReadBusState(bus);
    CriticalSection_Delay(410);

    __exit_critical_section();
    return !bit;
}

/*******************************************************************************
 * High Level Functions.
 ******************************************************************************/

void OneWire_WriteByte(OneWire_Bus_t *bus, uint8_t data) {
    uint8_t i = 8;

    while (i--) {
        OneWire_WriteBit(bus, data & 0x01);
        data >>= 1;
    }
}

uint8_t OneWire_ReadByte(OneWire_Bus_t *bus) {
    uint8_t i, data = 0;

    for (i = 0; i < 8; i++) {
        data |= OneWire_ReadBit(bus) << i;
    }

    return data;
}

/**
 * Sends the READ ROM command and reads back the ROM id.
 *
 * @param result Array of 8 bytes where the id will be placed.
 */
void OneWire_ReadRom(OneWire_Bus_t *bus, uint8_t result[8]) {
    uint8_t i;

    OneWire_WriteByte(bus, ONE_WIRE_READ_ROM);
    for (i = 0; i < 8; i++) {
        result[i] = OneWire_ReadByte(bus);
    }
}

/**
 * Sends the SKIP ROM command to the 1-Wire bus.
 */
void OneWire_SkipRom(OneWire_Bus_t *bus) {
    OneWire_WriteByte(bus, ONE_WIRE_SKIP_ROM);
}

/**
 * Sends the MATCH ROM command and the ROM id to match against.
 *
 * @param rom Array of 8 bytes with ID to match against.
 */
void OneWire_MatchRom(OneWire_Bus_t *bus, uint8_t rom[]) {
    uint8_t i;

    OneWire_WriteByte(bus, ONE_WIRE_MATCH_ROM);
    for (i = 0; i < 8; i++) {
        OneWire_WriteByte(bus, rom[i]);
    }
}

/**
 * Sends the SEARCH ROM command and returns 1 ID found on the 1-Wire(R) bus.
 *
 * @param  bitPattern      A pointer to an 8 byte char array where the
 *                         discovered identifier will be placed. When
 *                         searching for several slaves, a copy of the
 *                         last found identifier should be supplied in
 *                         the array, or the search will fail.
 *
 * @param  lastDeviationBit   The bit position where the algorithm made a
 *                         choice the last time it was run. This argument
 *                         should be 0 when a search is initiated. Supplying
 *                         the return argument of this function when calling
 *                         repeatedly will go through the complete slave
 *                         search.
 *
 * @return The last bit position where there was a discrepancy between slave addresses the last time
 *         this function was run.
 *         Returns ONE_WIRE_ROM_SEARCH_FAILED if an error was detected
 *         (e.g. a device was connected to the bus during the search),
 *         or ONE_WIRE_ROM_SEARCH_FINISHED when there are no more devices to be discovered.
 */
OneWire_RomSearchResult_t OneWire_SearchRom(OneWire_Bus_t *bus, uint8_t *bitPattern, uint8_t *lastDeviationBit) {
    uint8_t newDeviationBit = 0,
            currentBit = 1,
            bitMask = 0x01,
            bitA, bitB;

    // Send SEARCH ROM command on the bus
    OneWire_WriteByte(bus, ONE_WIRE_SEARCH_ROM);

    // Walk through all 64 bits
    while (currentBit <= 64) {
        // Read bit from bus twice
        bitA = OneWire_ReadBit(bus);
        bitB = OneWire_ReadBit(bus);

        // Both bits 1 (Error)
        if (bitA && bitB) {
            return ONE_WIRE_ROM_SEARCH_FAILED;
        }
        // Bits A and B are different. All devices have the same bit here.
        else if (bitA ^ bitB) {
            // Set the bit in bitPattern to this value
            if (bitA) {
                (*bitPattern) |= bitMask;
            }
            else {
                (*bitPattern) &= ~bitMask;
            }
        }
        // Both bits 0
        else {
            // If this is where a choice was made the last time, a '1' bit is selected this time
            if (currentBit == *lastDeviationBit) {
                (*bitPattern) |= bitMask;
            }
            // For the rest of the id, '0' bits are selected when discrepancies occur
            else if (currentBit > *lastDeviationBit) {
                (*bitPattern) &= ~bitMask;
                newDeviationBit = currentBit;
            }
            // If current bit in bit pattern = 0, then this is out new deviation
            else if ( !(*bitPattern & bitMask)) {
                newDeviationBit = currentBit;
            }
            // IF the bit is already 1
            else {
                // Do nothing
            }
        }

        // Send the selected bit to the bus
        OneWire_WriteBit(bus, (*bitPattern) & bitMask);

        // Increment current bit
        currentBit++;

        // Adjust bitMask and bitPattern pointer
        bitMask <<= 1;
        if (!bitMask) {
            bitMask = 0x01;
            bitPattern++;
        }
    }

    *lastDeviationBit = newDeviationBit;

    return *lastDeviationBit ? ONE_WIRE_ROM_SEARCH_CONTINUE : ONE_WIRE_ROM_SEARCH_FINISHED;
}


/**
 * Completely scans 1-Wire bus.
 *
 * @param limit     Set the max count of devices on bus to be discovered.
 * @param result    ROMs of devices are placed in this array. byte[count][64].
 * @return          Count of actually found devices.
 */
OneWire_ScanResult_t OneWire_ScanBus(OneWire_Bus_t *bus, OneWire_Device_t *resultDevices, uint8_t devicesLimit) {
    OneWire_ScanResult_t result = {
            .count = 0,
            .devices = resultDevices,
            .error = ONE_WIRE_SCAN_OK
    };

    uint8_t i,
            *newRom          = resultDevices[0].rom,
            *currentRom      = newRom,
            lastDeviationBit = 0;

    if (!devicesLimit) {
        result.error = ONE_WIRE_SCAN_ERROR_ZERO_DEVICES_LIMIT;
        return result;
    }

    // Initialize all addresses as zero (does not exist).
    // Each device address ROM is 8 bytes (64 bits) long
    for (i = 0; i < devicesLimit; i++) {
        memset(&resultDevices[i], 0, sizeof(OneWire_Device_t));
    }

    // Do slave search and place identifiers
    do {
        // Check presence
        if (OneWire_ResetAndDetectPresence(bus)) {
            memcpy(newRom, currentRom, 8);

            // Search next ROM
            if (OneWire_SearchRom(bus, newRom, &lastDeviationBit) == ONE_WIRE_ROM_SEARCH_FAILED) {
                result.error = ONE_WIRE_SCAN_ERROR_ROM_SEARCH_FAILED;
                return result;
            }

            // Set device bus and validate ROM CRC
            resultDevices[result.count].bus = bus;
            resultDevices[result.count].crcValid = OneWire_ValidateRomCRC(resultDevices[result.count].rom);

            // Not critical error
            if (!resultDevices[result.count].crcValid) {
                result.error = ONE_WIRE_SCAN_ERROR_INVALID_ROM_CRC;
            }

            // Move to the next device
            currentRom = newRom;
            result.count++;
            newRom = resultDevices[result.count].rom;
        }
        else {
            result.error = ONE_WIRE_SCAN_ERROR_NO_PRESENCE;
        }
    }
    while (lastDeviationBit != ONE_WIRE_ROM_SEARCH_FINISHED && result.count < devicesLimit);

    return result;
}

/**
 * Scans 1-Wire bus only for devices from the specified family.
 *
 * @param familyCode    Family code defines the lower byte of device ROM.
 * @param limit         Set the max count of devices on bus to be discovered.
 * @param result        ROMs of devices are placed in this array. byte[count][64].
 * @return              Count of actually found devices.
 */
OneWire_ScanResult_t OneWire_ScanFamily(uint8_t familyCode, OneWire_Device_t *resultDevices, uint8_t devicesLimit) {
    OneWire_ScanResult_t result;

    //todo !!! Write function

    devicesLimit = familyCode;
    familyCode = devicesLimit;
    result.devices = resultDevices;
    result.count = 0;
    result.error = ONE_WIRE_SCAN_ERROR_ROM_SEARCH_FAILED;

    return result;
}

/**
 * Compute the CRC8 or DOW-CRC of `data` using `seed` as inital value for the CRC.
 *
 * @param data  One byte of data to compute CRC from.
 * @param seed  The starting value of the CRC.
 *              Setting seed to 0 computes the crc8 of the `data`
 *
 * @return      The CRC8 of `data` with `seed` as initial value.
 *
 *  \note   Constantly passing the return value of this function
 *          As the seed argument computes the CRC8 value of a
 *          longer string of data.
 */
uint8_t OneWire_CRC8(uint8_t data, uint8_t seed) {
    uint8_t bitsLeft = 8,
            temp;

    while (bitsLeft--) {
        temp = ((seed ^ data) & 0x01);
        if (temp == 0) {
            seed >>= 1;
        }
        else {
            seed ^= 0x18;
            seed >>= 1;
            seed |= 0x80;
        }
        data >>= 1;
    }

    return seed;
}

/**
 * Calculate and check the CRC of a 64 bit ROM identifier.
 *
 * This function computes the CRC8 value of the first 56 bits of a 64 bit identifier.
 * It then checks the calculated value against the CRC value stored in ROM.
 *
 * @param  rom    A pointer to an array holding a 64 bit identifier.
 *
 * @return 1  The CRC's matched.
 *         0  There was a discrepancy between the calculated and the stored CRC.
 */
uint8_t OneWire_ValidateRomCRC(uint8_t *rom) {
    uint8_t i,
            crc8 = 0;

    // Calculate CRC of 7 bytes
    for (i = 0; i < 7; i++) {
        crc8 = OneWire_CRC8(*rom, crc8);
        rom++;
    }

    // Compare calculated CRC to 8th byte of ROM.
    return crc8 == *rom;
}

/**
 *
 */
void OneWire_rom2hex(const uint8_t rom[8], char hex[16]) {
    uint8_t i;
    const char *characters = "0123456789ABCDEF";

    for (i = 0; i < 8; i ++) {
        *hex++ = characters[(*rom >> 4) & 0x0F];
        *hex++ = characters[*rom++ & 0x0F];
    }
}

void OneWire_hex2rom(const char hex[16], uint8_t rom[8]) {
    uint8_t i;

    for (i = 0; i < 8; i++) {
        if (*hex >= '0' && *hex <= '9') {
            *rom = (*hex - '0') << 4;
        }
        else if (*hex >= 'A' && *hex <= 'F') {
            *rom = (*hex - 'A' + 10) << 4;
        }
        else {
            *rom = 0;
        }
        hex++;

        if (*hex >= '0' && *hex <= '9') {
            *rom |= *hex - '0';
        }
        else if (*hex >= 'A' && *hex <= 'F') {
            *rom |= *hex - 'A' + 10;
        }
        else {
            *rom = 0;
        }
        hex++;

        rom++;
    }
}

