/**
 * Thermometer.h
 */

#ifndef SRC_THERMOMETER_H_
#define SRC_THERMOMETER_H_

#include "../DS18B20/DS18B20.h"

/**
 * Undefined temperature value.
 */
#define TMTR_TEMPERATURE_UNDEFINED  DS18B20_TEMPERATURE_UNDEFINED

/**
 * Thermometer device statuses.
 */
typedef enum {
    TMTR_THERMOMETER_STATUS_NOT_CONNECTED = 0,
    TMTR_THERMOMETER_STATUS_CONNECTED,
    TMTR_THERMOMETER_STATUS_READY
} TMTR_ThermometerStatus_t;

/**
 * Thermometer device:
 * - device - 1-Wire device
 * - bootstrapProgress - count of measurements done after start up
 * - current status (connected, not available)
 * - measurement value
 */
typedef struct {
    char *name;
    OneWire_Device_t *device;
    uint8_t bootstrapProgress;
    TMTR_ThermometerStatus_t status;
    float value;
} TMTR_Thermometer_t;

/**
 * Initialize and reset sensors.
 */
void TMTR_Init(TMTR_Thermometer_t *_thermometers, uint8_t _thermometersCount);

void TMTR_AttachOneWireDevice(TMTR_Thermometer_t *thermometer, OneWire_Device_t *oneWireDevice);
void TMTR_DetachOneWireDevice(TMTR_Thermometer_t *thermometer);

TMTR_Thermometer_t* TMTR_GetThermometerByAttachedOneWireDevice(OneWire_Device_t *oneWireDevice);

/**
 * Collects one measure from all initialized and bounded devices.
 */
void TMTR_RefreshTemperature(void);

#endif /* SRC_THERMOMETER_H_ */
