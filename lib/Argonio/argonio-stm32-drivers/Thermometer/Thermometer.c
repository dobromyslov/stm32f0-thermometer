/**
 * Thermometer.c
 */

#include "Thermometer.h"

#include <stdlib.h>
#include "argonio_stm32_drivers_conf.h"

/**
 * Sensors list.
 */
static TMTR_Thermometer_t *thermometers;
static uint8_t thermometersCount;

void __resetThermometerStatus(TMTR_Thermometer_t *thermometer) {
    thermometer->bootstrapProgress = 0;
    thermometer->status = TMTR_THERMOMETER_STATUS_NOT_CONNECTED;
    thermometer->value  = TMTR_TEMPERATURE_UNDEFINED;
}

void TMTR_Init(TMTR_Thermometer_t *_thermometers, uint8_t _thermometersCount) {
    int i;

    thermometers = _thermometers;
    thermometersCount = _thermometersCount;

    /**
    * Initialize devices.
    */
    for (i = 0; i < thermometersCount; i++) {
        thermometers[i].device = NULL;
        __resetThermometerStatus(&thermometers[i]);
    }
}

void TMTR_AttachOneWireDevice(TMTR_Thermometer_t *thermometer, OneWire_Device_t *oneWireDevice) {
    // Check if the specified 1-Wire device already attached
    TMTR_Thermometer_t *attached = TMTR_GetThermometerByAttachedOneWireDevice(oneWireDevice);
    if (attached) {
        TMTR_DetachOneWireDevice(attached);
    }
    else {
        __resetThermometerStatus(thermometer);
    }

    thermometer->device = oneWireDevice;
}

void TMTR_DetachOneWireDevice(TMTR_Thermometer_t *thermometer) {
    if (thermometer->device != NULL) {
        thermometer->device = NULL;
        __resetThermometerStatus(thermometer);
    }
}

TMTR_Thermometer_t* TMTR_GetThermometerByAttachedOneWireDevice(OneWire_Device_t *oneWireDevice) {
    uint8_t i;

    // Find One Wire device with the ROM specified in sensor configuration
    for (i = 0; i < thermometersCount; i++) {
        if (thermometers[i].device == oneWireDevice) {
            return &thermometers[i];
        }
    }

    return NULL;
}

void TMTR_RefreshTemperature(void) {
    uint8_t i;
    DS18B20_GetTResult_t measureResult;

    for (i = 0; i < thermometersCount; i++) {
        TMTR_Thermometer_t *thermometer = &thermometers[i];
        if (thermometer->status == TMTR_THERMOMETER_STATUS_NOT_CONNECTED) {
            continue;
        }

        measureResult = DS18B20_GetT(thermometer->device);

        if (measureResult.error == DS18B20_GETT_OK) {
            if (thermometer->bootstrapProgress >= TMTR_SKIP_MEASURES_COUNT) {
                if (thermometer->value == TMTR_TEMPERATURE_UNDEFINED) {
                    thermometer->value = measureResult.value;
                }
                else {
                    thermometer->value = (thermometer->value + measureResult.value * 2) / 3;
                }
            }

            if (thermometer->status != TMTR_THERMOMETER_STATUS_READY) {
                thermometer->bootstrapProgress++;
            }

            if (thermometer->bootstrapProgress >= TMTR_SKIP_MEASURES_COUNT + TMTR_BOOTSTRAP_MEASURES_COUNT) {
                thermometer->status = TMTR_THERMOMETER_STATUS_READY;
            }
        }
        else {
            __resetThermometerStatus(thermometer);
        }
    }
}

