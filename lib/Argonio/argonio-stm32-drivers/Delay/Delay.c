#include "Delay.h"
#include "argonio_stm32_drivers_conf.h"

/**
 * Delay in microseconds.
 *
 * This function requires proper value of DELAY_MIN and DELAY_COEF
 */
void CriticalSection_Delay(unsigned int us) {
    // Function call itself takes not less than MINIMAL_DELAY microseconds
    if (us > DELAY_MIN) {
        us = (us - DELAY_MIN) * DELAY_COEF;
        __ASM volatile(
                " mov r0, %[us] \n\t"
                        ".syntax unified \n\t"
                        "1: subs r0, #1 \n\t"
                        ".syntax divided \n\t"
                        " bhi 1b \n\t"
        :
        : [us] "r" (us)
        : "r0"
        );
    }
}
