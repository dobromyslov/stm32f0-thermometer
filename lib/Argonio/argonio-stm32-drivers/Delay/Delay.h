/**
 * Delay function to use in critical sections without interruptions.
 *
 * For example it's used in 1-Wire protocol to achieve mandatory signal timing.
 */
#ifndef STM32F0_THERMOMETER_DELAY_H
#define STM32F0_THERMOMETER_DELAY_H

/**
 * Delay in microseconds.
 *
 * This function requires proper value of DELAY_MIN and DELAY_COEF specified in argonio_stm32_drivers_conf.h
 */
void CriticalSection_Delay(unsigned int us);

#endif //STM32F0_THERMOMETER_DELAY_H
