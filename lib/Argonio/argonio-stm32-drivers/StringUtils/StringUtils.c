/*
 * StringUtils.c
 */
#include "StringUtils.h"

#include <stdarg.h>
#include <string.h>

uint8_t getIntLength(int n) {
    uint8_t sign = 0;
    if (n  < 0) {
        sign = 1;
        n *= -1;
    }

    if (n < 100000) {
        // 5 or less
        if (n < 100) {
            // 1 or 2
            if (n < 10)
                return sign + 1;
            else
                return sign + 2;
        }
        else {
            // 3 or 4 or 5
            if (n < 1000)
                return sign + 3;
            else {
                // 4 or 5
                if (n < 10000)
                    return sign + 4;
                else
                    return sign + 5;
            }
        }
    }
    else {
        // 6 or more
        if (n < 10000000) {
            // 6 or 7
            if (n < 1000000)
                return sign + 6;
            else
                return sign + 7;
        }
        else {
             // 8 to 10
             if (n < 100000000)
                 return sign + 8;
             else {
                 // 9 or 10
                 if (n < 1000000000)
                     return sign + 9;
                 else
                     return sign + 10;
             }
        }
    }
}

uint8_t getShortLength(short n) {
    uint8_t sign = 0;
    if (n < 0) {
        sign = 1;
        n *= -1;
    }

    if (n < 100) {
        // 1 or 2
        if (n < 10) {
            return sign + 1;
        }
        else {
            return sign + 2;
        }
    }
    else {
        if (n < 10000) {
            if (n < 1000) {
                return sign + 3;
            }
            else {
                return sign + 4;
            }
        }
        else {
            return sign + 5;
        }
    }
}

uint8_t getCharLength(char n) {
    uint8_t sign = 0;
    if (n < 0) {
        sign = 1;
        n *= -1;
    }

    if (n < 100) {
        if (n < 10) {
            return sign + 1;
        }
        else {
            return sign + 2;
        }
    }
    else {
        return sign + 3;
    }
}

uint8_t writeInt(char *dest, int n, char padSign) {
    uint8_t l = getIntLength(n);
    if (n < 0) {
        *dest = '-';
        n *= -1;
    }
    else if (padSign) {
        *dest = ' ';
        l++;
    }

    dest += l - 1;
    do {
        *dest-- = '0' + (char)(n % 10);
        n /= 10;
    }
    while (n);

    return l;
}

uint8_t str_append(char *dest, char *str) {
    uint8_t result = 0;

    // look for null terminator
    while (*dest != 0) {
        dest++;
    }

    // append str to dest
    uint8_t c;
    while (c = *str) {
        *dest++ = *str++;
        result++;
    }

    // append null terminator
    *dest = 0;

    return result;
}

uint8_t str_append_int(char *dest, int n, char padSign) {
    while (*dest != 0) {
        dest++;
    }

    uint8_t l = writeInt(dest, n, padSign);
    dest += l;
    *dest = 0;
    return l;
}

uint8_t str_append_int_pad_zero(char *dest, int n, char padToLength) {
    while (*dest != 0) {
        dest++;
    }

    uint8_t l = getIntLength(n);
    uint8_t padSize;
    for (padSize = padToLength - l; padSize > 0; padSize--) {
        *dest = '0';
        dest++;
    }

    l = writeInt(dest, n, 0);
    dest += l;
    *dest = 0;
    return l;
}

uint8_t str_concat(char *dest, char count, ...) {
    uint8_t result = 0;

    // look for null terminator
    while (*dest != 0) {
        dest++;
    }

    // append to dest
    char *arg;
    va_list ap;
    va_start(ap, count);
    uint8_t c;
    for (;count > 0; count--) {
        arg = (char*)va_arg(ap, char*);
        while (c = *arg) {
            *dest++ = *arg++;
            result++;
        }
    }
    va_end(ap);

    // append null terminator
    *dest = 0;

    return result;
}
