/*
 * StringUtils.h
 */

#ifndef STRINGUTILS_H_
#define STRINGUTILS_H_

#include <stdint.h>

uint8_t writeInt(char *dest, int n, char padSign);
char * intToString(int n);
uint8_t str_append(char *dest, char *str);
uint8_t str_append_int(char *dest, int n, char padSign);
uint8_t str_append_int_pad_zero(char *dest, int n, char padToLength);
uint8_t str_concat(char *dest, char count, ...);

#endif /* STRINGUTILS_H_ */
