STM32F0 1-Wire Thermometer DS18B20
==================================

Project features:

* 1-Wire protocol
* DS18B20 sensor support
* Tested on STM32F0Discovery (STM32F051R8)
* Makefile build from console and Eclipse
* CMSIS v4.1 - http://www.arm.com/products/processors/cortex-m/cortex-microcontroller-software-interface-standard.php
* STM32CubeF0 HAL v1.0.1 - http://www.st.com/web/en/catalog/tools/PF260612
* FreeRTOS v7.6 integrated with CMSIS-RTOS from STM32Cube
* OpenOCD instant flash support with `make flash`
* Works great with Eclipse debug via GDB - see how to import project http://www.chibios.org/dokuwiki/doku.php?id=chibios:guides:eclipse2