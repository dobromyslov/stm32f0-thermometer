//
// Created by bbg on 09.01.16.
//

#ifndef STM32F0_THERMOMETER_MENU_H
#define STM32F0_THERMOMETER_MENU_H

#include "Hardware.h"

typedef struct {
    char *name;
    void (*draw)();
    void (*processKeyboard)(MatrixKeyboard_Device_t *keyboard);
} Menu_Page_t;

void Menu_draw();
void Menu_processKeyboard(MatrixKeyboard_Device_t *keyboard);

#endif //STM32F0_THERMOMETER_MENU_H
