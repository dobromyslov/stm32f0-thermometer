//
// Created by bbg on 09.01.16.
//

#include "Hardware.h"

RTC_HandleTypeDef RtcHandle;
I2C_HandleTypeDef  hi2c1;
UART_HandleTypeDef huart1;

__IO ITStatus UartReady = RESET;
unsigned short uartRxBufferCount = 0;
char uartRxChar = 0;

OneWire_Bus_t oneWireBus = {
        .port      = GPIOA,
        .pin       = GPIO_PIN_15,
        .pinNumber = 15
};
OneWire_Device_t oneWireDevices[ONE_WIRE_DEVICES_LIMIT];
uint8_t oneWireDevicesCount = 0;

TMTR_Thermometer_t thermometers[THERMOMETERS_COUNT] = {
        {.name = "In"},
        {.name = "Out"},
        {.name = "Heater 1"},
        {.name = "Heater 2"}
};

TMTR_Thermometer_t *inThermometer    = &thermometers[0];
TMTR_Thermometer_t *outThermometer    = &thermometers[1];
TMTR_Thermometer_t *heater1Thermometer = &thermometers[2];
TMTR_Thermometer_t *heater2Thermometer = &thermometers[3];

MatrixKeyboard_Device_t mainKeyboard = {
        .rows = 4,
        .cols = 4,
        .scanPins = (MatrixKeyboard_Pin_t[]){
                {.port = GPIOA, .pin = GPIO_PIN_7, .pinNumber = 7},
                {.port = GPIOA, .pin = GPIO_PIN_6, .pinNumber = 6},
                {.port = GPIOA, .pin = GPIO_PIN_5, .pinNumber = 5},
                {.port = GPIOA, .pin = GPIO_PIN_4, .pinNumber = 4}
        },
        .readPins = (MatrixKeyboard_Pin_t[]){
                {.port = GPIOA, .pin = GPIO_PIN_3, .pinNumber = 3},
                {.port = GPIOA, .pin = GPIO_PIN_2, .pinNumber = 2},
                {.port = GPIOA, .pin = GPIO_PIN_1, .pinNumber = 1},
                {.port = GPIOA, .pin = GPIO_PIN_0, .pinNumber = 0}
        },
        .keyboardBuffer = (uint8_t[]){0, 0, 0, 0},
        .keyboardEvents = (MatrixKeyboard_KeyEvent_t[]){
                0,0,0,0,
                0,0,0,0,
                0,0,0,0,
                0,0,0,0
        },
        .pressedKeysCount = 0
};

Relay_Device_t relays[RELAYS_COUNT] = {
        {.port = GPIOB, .pin = GPIO_PIN_0, .pinNumber = 0},
        {.port = GPIOB, .pin = GPIO_PIN_1, .pinNumber = 1},
        {.port = GPIOB, .pin = GPIO_PIN_10, .pinNumber = 10},
        {.port = GPIOB, .pin = GPIO_PIN_11, .pinNumber = 11},
};

/**
 * System Clock Configuration
 */
void SystemClock_Config(void) {
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_PeriphCLKInitTypeDef PeriphClkInit;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
    RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    RCC_OscInitStruct.OscillatorType =  RCC_OSCILLATORTYPE_LSI | RCC_OSCILLATORTYPE_LSE;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
#ifdef USE_RTC_LSI
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_OFF;
#else
    RCC_OscInitStruct.LSIState = RCC_LSI_OFF;
    RCC_OscInitStruct.LSEState = RCC_LSE_ON;
#endif
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1);

    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_RTC;
    PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
    PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_SYSCLK;
#ifdef USE_RTC_LSI
    PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
#else
    PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
#endif
    HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

    __SYSCFG_CLK_ENABLE();
}

/**
 * Real Time Clock init.
 */
void MX_RTC_Init(void) {
    RtcHandle.Instance = RTC;
    RtcHandle.Init.HourFormat     = RTC_HOURFORMAT_24;
    RtcHandle.Init.AsynchPrediv   = RTC_PREDIV_A;
    RtcHandle.Init.SynchPrediv    = RTC_PREDIV_S;
    RtcHandle.Init.OutPut         = RTC_OUTPUT_DISABLE;
    RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
    RtcHandle.Init.OutPutType     = RTC_OUTPUT_TYPE_OPENDRAIN;

    if (HAL_RTC_Init(&RtcHandle) != HAL_OK) {
        Error_Handler();
    }
}

/**
 * Configure pins as
 * Analog
 * Input
 * Output
 * EVENT_OUT
 * EXTI
 */
void MX_GPIO_Init(void) {
    GPIO_InitTypeDef GPIO_InitStruct;

    /* GPIO Ports Clock Enable */
    __GPIOA_CLK_ENABLE();
    __GPIOB_CLK_ENABLE();
    __GPIOC_CLK_ENABLE();

    // DMA Clock Enable
    __DMA1_CLK_ENABLE();

    /**
     * PC13 - Led
     */
    GPIO_InitStruct.Pin   = GPIO_PIN_13;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

/**
 * UART bus init.
 */
void MX_UART1_Init(void) {
    huart1.Instance            = USART1;
    huart1.Init.BaudRate       = 19200; //115200;
    huart1.Init.WordLength     = UART_WORDLENGTH_8B;
    huart1.Init.StopBits       = UART_STOPBITS_1;
    huart1.Init.Parity         = UART_PARITY_NONE;
    huart1.Init.Mode           = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl      = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling   = UART_OVERSAMPLING_16;
    huart1.Init.OneBitSampling = UART_ONEBIT_SAMPLING_DISABLED ;
    huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    if (HAL_UART_Init(&huart1) != HAL_OK) {
        Error_Handler();
    }
}

/**
 * I2C1 bus init.
 */
void MX_I2C1_Init(void) {
    hi2c1.Instance = I2C1;
    hi2c1.Init.Timing = 0x20303E5D;
    hi2c1.Init.OwnAddress1 = 0;
    hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
    hi2c1.Init.OwnAddress2 = 0;
    hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
    hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;

    HAL_StatusTypeDef result = HAL_I2C_Init(&hi2c1);
    if (result != HAL_OK) {
        //Console_log("I2C error: " + result);
    }
    //Console_log("I2C error: " + result);

    //Configure Analogue filter
    HAL_I2CEx_AnalogFilter_Config(&hi2c1, I2C_ANALOGFILTER_ENABLED);
}

void Hardware_Init(void) {
    // Reset of all peripherals, Initializes the Flash interface and the Systick
    HAL_Init();

    // Configure the system clock
    SystemClock_Config();

    // System interrupt init
    HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

    // RTC Init
    MX_RTC_Init();

    // Initialize all configured peripherals
    MX_GPIO_Init();

    // Init UART bus
    MX_UART1_Init();
    Console_init(&huart1);
    Console_log("\r\nUART1 initialized");

    // Init I2C bus
    MX_I2C1_Init();
    Console_log("I2C initialized");

    // Initialize LCD
    LCD_initialize(&hi2c1);

    // Initialize matrix keyboard
    MatrixKeyboard_init(&mainKeyboard);

    // Initialize 1-Wire
    OneWire_Init(&oneWireBus);

    // Initialize Thermometers
    TMTR_Init(thermometers, THERMOMETERS_COUNT);

    // Initialize switching relays
    Relay_Init(relays, RELAYS_COUNT);
}
