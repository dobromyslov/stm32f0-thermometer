/**
 * stm32f0xx_hal_msp.c
 *
 * MSP Initialization and de-Initialization codes for STM32 HAL.
 */

#include "stm32f0xx.h"
#include "Hardware.h"
#include "Configuration.h"

/**
 * I2C1
 *
 * PB6      I2C1_SCL
 * PB7      I2C1_SDA
 */
#define I2C1_GPIOx  GPIOB
#define I2C1_SCL    GPIO_PIN_6
#define I2C1_SDA    GPIO_PIN_7

/**
 * USART1
 *
 * PA9      USART1_TX
 * PA10     USART1_RX
 */
#define USART1_GPIOx    GPIOA
#define USART1_TX       GPIO_PIN_9
#define USART1_RX       GPIO_PIN_10


/**
  * @brief RTC MSP Initialization
  *        This function configures the hardware resources used in this example
  * @param hrtc: RTC handle pointer
  *
  * @note  Care must be taken when HAL_RCCEx_PeriphCLKConfig() is used to select
  *        the RTC clock source; in this case the Backup domain will be reset in
  *        order to modify the RTC Clock source, as consequence RTC registers (including
  *        the backup registers) and RCC_BDCR register are set to their reset values.
  *
  * @retval None
  */
void HAL_RTC_MspInit(RTC_HandleTypeDef *hrtc) {
    RCC_OscInitTypeDef        RCC_OscInitStruct;
    RCC_PeriphCLKInitTypeDef  PeriphClkInitStruct;

    /*##-1- Enables the PWR Clock and Enables access to the backup domain ###################################*/
    /* To change the source clock of the RTC feature (LSE, LSI), You have to:
     - Enable the power clock using __PWR_CLK_ENABLE()
     - Enable write access using HAL_PWR_EnableBkUpAccess() function before to
       configure the RTC clock source (to be done once after reset).
     - Reset the Back up Domain using __HAL_RCC_BACKUPRESET_FORCE() and
       __HAL_RCC_BACKUPRESET_RELEASE().
     - Configure the needed RTC clock source */
    __PWR_CLK_ENABLE();
    HAL_PWR_EnableBkUpAccess();

    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
#ifdef USE_RTC_LSI
    PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
#else
#ifdef USE_RTC_LSE
    PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
#endif
#endif
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK) {
        Error_Handler();
    }

    /*##-2- Enable RTC peripheral Clocks #######################################*/
    /* Enable RTC Clock */
    __HAL_RCC_RTC_ENABLE();

    /*##-4- Configure the NVIC for RTC Alarm ###################################*/
    HAL_NVIC_SetPriority(RTC_IRQn, 0x0F, 0);
    HAL_NVIC_EnableIRQ(RTC_IRQn);
}

/**
 * RTC DeInit.
 */
void HAL_RTC_MspDeInit(RTC_HandleTypeDef *hrtc) {
   __HAL_RCC_RTC_DISABLE();
}

/**
 * I2C Init.
 *
 *
 */
void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c) {

    GPIO_InitTypeDef GPIO_InitStruct;
    if (hi2c->Instance == I2C1) {
        // Peripheral clock enable
        __I2C1_CLK_ENABLE();

        /**
         * I2C1 GPIO Configuration
         */
        GPIO_InitStruct.Pin       = I2C1_SCL | I2C1_SDA;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
        GPIO_InitStruct.Pull      = GPIO_NOPULL;
        GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF1_I2C1;
        HAL_GPIO_Init(I2C1_GPIOx, &GPIO_InitStruct);
    }
}

/**
 * I2C DeInit.
 */
void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c) {

    if (hi2c->Instance == I2C1) {
        // Peripheral clock disable
        __I2C1_CLK_DISABLE();

        // I2C1 GPIO Configuration
        HAL_GPIO_DeInit(I2C1_GPIOx, I2C1_SCL | I2C1_SDA);
    }
}

/**
 * UART Init.
 */
void HAL_UART_MspInit(UART_HandleTypeDef* huart) {

    static DMA_HandleTypeDef hdma_tx;
    static DMA_HandleTypeDef hdma_rx;


    GPIO_InitTypeDef GPIO_InitStruct;
    if (huart->Instance == USART1) {
        // Peripheral clock enable
        __USART1_CLK_ENABLE();

        // USART1 GPIO Configuration
        GPIO_InitStruct.Pin       = USART1_RX | USART1_TX;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull      = GPIO_PULLUP;
        GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF1_USART1;
        HAL_GPIO_Init(USART1_GPIOx, &GPIO_InitStruct);

        // TX DMA config
        hdma_tx.Instance                 = DMA1_Channel2;
        hdma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
        hdma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
        hdma_tx.Init.MemInc              = DMA_MINC_ENABLE;
        hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
        hdma_tx.Init.Mode                = DMA_NORMAL;
        hdma_tx.Init.Priority            = DMA_PRIORITY_LOW;
        HAL_DMA_Init(&hdma_tx);
        __HAL_LINKDMA(huart, hdmatx, hdma_tx);

        // RX DMA config
        hdma_rx.Instance                 = DMA1_Channel3;
        hdma_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
        hdma_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
        hdma_rx.Init.MemInc              = DMA_MINC_ENABLE;
        hdma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
        hdma_rx.Init.Mode                = DMA_NORMAL;
        hdma_rx.Init.Priority            = DMA_PRIORITY_HIGH;
        HAL_DMA_Init(&hdma_rx);
        __HAL_LINKDMA(huart, hdmarx, hdma_rx);

        // TX DMA Transfer complete interrupts
        HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 1);
        HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

        // RX DMA Transfer complete interrupts
        //HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
        //HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);
    }
}

/**
 * UART DeInit.
 */
void HAL_UART_MspDeInit(UART_HandleTypeDef* huart) {

    if (huart->Instance == USART1) {
        // Peripheral clock disable
        __USART1_CLK_DISABLE();

        // USART1 GPIO Configuration
        HAL_GPIO_DeInit(USART1_GPIOx, USART1_RX | USART1_TX);

        // Disable DMA
        if (huart->hdmarx != 0) {
            HAL_DMA_DeInit(huart->hdmarx);
        }

        if (huart->hdmatx != 0) {
            HAL_DMA_DeInit(huart->hdmatx);
        }

        HAL_NVIC_DisableIRQ(DMA1_Channel2_3_IRQn);
        HAL_NVIC_DisableIRQ(DMA1_Channel2_3_IRQn);
    }
}
