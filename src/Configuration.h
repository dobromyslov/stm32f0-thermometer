#ifndef SRC_CONFIGURATION_H_
#define SRC_CONFIGURATION_H_

#include "stm32f0xx_hal_eeprom_emu.h"

/**
 * Main configuration.
 */
typedef struct {
    /**
     * Indoor temperature settings.
     */
    uint8_t inT;
    uint8_t inTDelta;

    /**
     * Heater 1 temperature settings.
     */
    uint8_t heater1T;
    uint8_t heater1Delta;

    /**
     * Heater 2 temperature settings.
     */
    uint8_t heater2T;
    uint8_t heater2Delta;

    /**
     * Heater 1 and 2 enable flag.
     */
    uint8_t heater1Enable;
    uint8_t heater2Enable;

    /**
     * Indoor 1-Wire temperature sensor ROM.
     */
    uint8_t inRom[8];

    /**
     * Heater 1 1-Wire temperature sensor ROM.
     */
    uint8_t heater1Rom[8];

    /**
     * Heater 2 1-Wire temperature sensor ROM.
     */
    uint8_t heater2Rom[8];

    /**
     * Outdoor temperature sensor ROM.
     */
    uint8_t outRom[8];

    /**
     * Main heater enable flag.
     * It's not stored in EEPROM.
     * Reset to 0 on each start up.
     */
    uint8_t heaterEnable;
} Configuration_t;

/**
 * Configuration.
 */
extern Configuration_t configuration;

uint16_t Configuration_init();
uint8_t  Configuration_validate(uint8_t toDefault);
uint16_t Configuration_save();

#endif /* SRC_CONFIGURATION_H_ */
