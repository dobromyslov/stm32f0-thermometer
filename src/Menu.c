//
// Created by bbg on 09.01.16.
//

#include "Menu.h"
#include "conf/argonio_stm32_drivers_conf.h"
#include "Hardware.h"
#include "Configuration.h"

void Menu_Main_draw();
void Menu_Main_processKeyboard(MatrixKeyboard_Device_t *keyboard);

void Menu_IndoorHeater_draw();
void Menu_IndoorHeater_processKeyboard(MatrixKeyboard_Device_t *keyboard);

void Menu_Heater1_draw();
void Menu_Heater1_processKeyboard(MatrixKeyboard_Device_t *keyboard);

void Menu_Heater2_draw();
void Menu_Heater2_processKeyboard(MatrixKeyboard_Device_t *keyboard);

void Menu_Light_draw();
void Menu_Light_processKeyboard(MatrixKeyboard_Device_t *keyboard);

void Menu_Date_draw();
void Menu_Date_processKeyboard(MatrixKeyboard_Device_t *keyboard);

void Menu_OneWire_draw();
void Menu_OneWire_processKeyboard(MatrixKeyboard_Device_t *keyboard);

Menu_Page_t menuPages[] = {
        {.name = "Main", .draw = Menu_Main_draw, .processKeyboard = Menu_Main_processKeyboard},
        {.name = "Indoor Heater", .draw = Menu_IndoorHeater_draw, .processKeyboard = Menu_IndoorHeater_processKeyboard},
        {.name = "Heater 1", .draw = Menu_Heater1_draw, .processKeyboard = Menu_Heater1_processKeyboard},
        {.name = "Heater 2", .draw = Menu_Heater2_draw, .processKeyboard = Menu_Heater2_processKeyboard},
        {.name = "Light",    .draw = Menu_Light_draw,   .processKeyboard = Menu_Light_processKeyboard},
        {.name = "Date and Time", .draw = Menu_Date_draw, .processKeyboard = Menu_Date_processKeyboard},
        {.name = "One Wire", .draw = Menu_OneWire_draw, .processKeyboard = Menu_OneWire_processKeyboard}
};

void Menu_draw() {
    LCD_buffer_clear(&hi2c1);

    menuPages[0].draw();

    LCD_refresh(&hi2c1);
}

void Menu_processKeyboard(MatrixKeyboard_Device_t *keyboard) {
    menuPages[0].processKeyboard(keyboard);
}

char line[LCD_COLS + 1] = {[LCD_COLS] = 0};

void printTemperature(float t) {
    if (t != TMTR_TEMPERATURE_UNDEFINED) {
        str_append_int(line, t, 1);
    }
    else {
        str_append(line, "N/A");
    }
}

void printTemperatureInterval(float start, float end) {
    str_append(line, "<");
    str_append_int(line, (int)start, 0);
    str_append(line, "-");
    str_append_int(line, (int)end, 0);
    str_append(line, ">");
}

void printTime() {
    RTC_DateTypeDef sdatestructureget;
    RTC_TimeTypeDef stimestructureget;

    HAL_RTC_GetTime(&RtcHandle, &stimestructureget, FORMAT_BIN);
    HAL_RTC_GetDate(&RtcHandle, &sdatestructureget, FORMAT_BIN);

    str_append_int_pad_zero(line, stimestructureget.Hours, 2);
    str_append(line, stimestructureget.Seconds % 2 == 0 ? ":" : " ");
    str_append_int_pad_zero(line, stimestructureget.Minutes, 2);
}

void printOnOff(uint8_t enable) {
    str_append(line, enable ? "Yes" : " No");
}

void printRom(uint8_t rom[8]) {
    char hexRom[17] = {[16] = 0};

    OneWire_rom2hex(rom, hexRom);
    str_append(line, hexRom);
}

/***********************************************************************************************************************
 * Main
 **********************************************************************************************************************/
void Menu_Main_draw() {
    LCD_setBlink(&hi2c1, 0);

    // 1st line
    strcpy(line, "In :");
    printTemperature(inThermometer->value);
    str_append(line, " ");
    printTemperatureInterval(configuration.inT - configuration.inTDelta, configuration.inT);
    LCD_buffer_print(0, 0, line);

    // 2nd line
    strcpy(line, "H1 :");
    printTemperature(heater1Thermometer->value);
    str_append(line, " ");
    printTemperatureInterval(configuration.heater1T - configuration.heater1Delta, configuration.heater1T);
    LCD_buffer_print(1, 0, line);

    // 3rd line
    strcpy(line, "H2 :");
    printTemperature(heater2Thermometer->value);
    str_append(line, " ");
    printTemperatureInterval(configuration.heater2T - configuration.heater2Delta, configuration.heater2T);
    LCD_buffer_print(2, 0, line);

    // 4th line
    strcpy(line, "Out:");
    printTemperature(outThermometer->value);
    LCD_buffer_print(3, 0, line);

    // Print date and time
    strcpy(line, "");
    printTime();
    LCD_buffer_print(3, 15, line);
}

void Menu_Main_processKeyboard(MatrixKeyboard_Device_t *keyboard) {

}

/***********************************************************************************************************************
 * Indoor heater settings
 **********************************************************************************************************************/
void Menu_IndoorHeater_draw() {

    // 1st line
    strcpy(line, "Indoor Heater");
    LCD_buffer_print(0, 0, line);

    // 2nd line
    strcpy(line, "ROM:");
    printRom(configuration.inRom);
    LCD_buffer_print(1, 0, line);

    // 3rd line
    strcpy(line, "Tmax:");
    printTemperature(configuration.inT);
    str_append(line, " Tmin:");
    printTemperature(configuration.inT - configuration.inTDelta);
    LCD_buffer_print(2, 0, line);

    // 4th line
    strcpy(line, "Enable: ");
    printOnOff(configuration.heaterEnable);
    LCD_buffer_print(3, 0, line);

    LCD_setBlink(&hi2c1, 1);
}

void Menu_IndoorHeater_processKeyboard(MatrixKeyboard_Device_t *keyboard) {

}

/***********************************************************************************************************************
 * Heater 1 settings
 **********************************************************************************************************************/
void Menu_Heater1_draw() {
    // 1st line
    strcpy(line, "Heater 1");
    LCD_buffer_print(0, 0, line);

    // 2nd line
    strcpy(line, "ROM:");
    printRom(configuration.heater1Rom);
    LCD_buffer_print(1, 0, line);

    // 3rd line
    strcpy(line, "Tmax:");
    printTemperature(configuration.heater1T);
    str_append(line, " Tmin:");
    printTemperature(configuration.inT - configuration.heater1Delta);
    LCD_buffer_print(2, 0, line);

    // 4th line
    strcpy(line, "Enable: ");
    printOnOff(configuration.heater1Enable);
    LCD_buffer_print(3, 0, line);

    LCD_setBlink(&hi2c1, 1);
}

void Menu_Heater1_processKeyboard(MatrixKeyboard_Device_t *keyboard) {

}

/***********************************************************************************************************************
 * Heater 2 settings
 **********************************************************************************************************************/
void Menu_Heater2_draw() {
    // 1st line
    strcpy(line, "Heater 2");
    LCD_buffer_print(0, 0, line);

    // 2nd line
    strcpy(line, "ROM:");
    printRom(configuration.heater2Rom);
    LCD_buffer_print(1, 0, line);

    // 3rd line
    strcpy(line, "Tmax:");
    printTemperature(configuration.heater2T);
    str_append(line, " Tmin:");
    printTemperature(configuration.inT - configuration.heater2Delta);
    LCD_buffer_print(2, 0, line);

    // 4th line
    strcpy(line, "Enable: ");
    printOnOff(configuration.heater2Enable);
    LCD_buffer_print(3, 0, line);

    LCD_setBlink(&hi2c1, 1);
}

void Menu_Heater2_processKeyboard(MatrixKeyboard_Device_t *keyboard) {

}

/***********************************************************************************************************************
 * Light settings
 **********************************************************************************************************************/
void Menu_Light_draw() {
    // 1st line
    strcpy(line, "Light");
    LCD_buffer_print(0, 0, line);

    // 2nd line
    strcpy(line, "LUXmin:");
    //todo
    LCD_buffer_print(1, 0, line);

    // 3rd line
    strcpy(line, "Time: ");
    //todo
    str_append(line, "06:30");
    str_append(line, " - ");
    //todo
    str_append(line, "22:00");
    LCD_buffer_print(2, 0, line);

    // 4th line
    strcpy(line, "Enable: ");
    //todo
    printOnOff(0);
    LCD_buffer_print(3, 0, line);

    LCD_setBlink(&hi2c1, 1);
}

void Menu_Light_processKeyboard(MatrixKeyboard_Device_t *keyboard) {

}

/***********************************************************************************************************************
 * Date and time settings
 **********************************************************************************************************************/
void Menu_Date_draw() {
    // 1st line
    strcpy(line, "Date and Time");
    LCD_buffer_print(0, 0, line);

    // 2nd line
    strcpy(line, "Date:");
    //todo
    str_append(line, "2016-01-01");
    LCD_buffer_print(1, 0, line);

    // 3rd line
    strcpy(line, "Time:");
    //todo
    str_append(line, "00:00");
    LCD_buffer_print(2, 0, line);

    // 4th line
    strcpy(line, "");
    //todo
    LCD_buffer_print(3, 0, line);

    LCD_setBlink(&hi2c1, 1);
}

void Menu_Date_processKeyboard(MatrixKeyboard_Device_t *keyboard) {

}

/***********************************************************************************************************************
 * 1-Wire settings
 **********************************************************************************************************************/
void Menu_OneWire_draw() {
    // 1st line
    strcpy(line, "1-Wire");
    LCD_buffer_print(0, 0, line);

    // 2nd line
    strcpy(line, "Status: ");
    //todo
    printOnOff(1);
    LCD_buffer_print(1, 0, line);

    // 3rd line
    strcpy(line, "Devices:");
    //todo
    str_append(line, "4");
    LCD_buffer_print(2, 0, line);

    // 4th line
    strcpy(line, "F2-details");
    //todo
    LCD_buffer_print(3, 0, line);

    LCD_setBlink(&hi2c1, 1);
}

void Menu_OneWire_processKeyboard(MatrixKeyboard_Device_t *keyboard) {

}
