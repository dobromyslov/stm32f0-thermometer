#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cmsis_os.h"

#include "Hardware.h"
#include "Configuration.h"
#include "Menu.h"

/**
 * Screens.
 */
#define SCREEN_MAIN 0
#define SCREEN_MENU 1

volatile uint8_t screen = 0;

/**
 * Menu.
 */
#define MENU_AIR1       0
#define MENU_AIR2       1
#define MENU_HEAT       2
#define MENU_ONE_WIRE   3

volatile uint8_t selectedMenu = 0;
volatile uint8_t selectedMenuParameter = 0;

/**
 * Heat steps.
 */
volatile uint8_t heatSteps = 0;

static int ledState = 0;

static void StartThread(void const * argument);
static void CheckButtonThread(void const * argument);
static void OneWireThread(void const * argument);
static void HeaterControlThread(void const * argument);

int main(void) {

    Hardware_Init();

    // Init configuration
    Configuration_init(&configuration);

    osThreadDef(Start_thread, StartThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
    osThreadDef(CheckButton_thread, CheckButtonThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
    osThreadDef(OneWire_thread, OneWireThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
    //osThreadDef(HeaterControl_thread, HeaterControlThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);

    osThreadCreate(osThread(Start_thread), NULL);
    osThreadCreate(osThread(CheckButton_thread), NULL);
    osThreadCreate(osThread(OneWire_thread), NULL);
    //osThreadCreate(osThread(HeaterControl_thread), NULL);

    // Run FreeRTOS
    Console_log("Start kernel");
    osKernelStart(NULL, NULL);

    return 0;
}

static void StartThread(void const * argument) {
    while(1) {
        vTaskDelay(200);
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, ledState);

        Menu_draw();
    }
}

/*
static void SetDateTime(void) {
    RTC_DateTypeDef  sdatestructure;
    RTC_TimeTypeDef  stimestructure;
    RTC_AlarmTypeDef salarmstructure;

    // Set Date: Tuesday February 18th 2014
    sdatestructure.Year = 0x14;
    sdatestructure.Month = RTC_MONTH_FEBRUARY;
    sdatestructure.Date = 0x18;
    sdatestructure.WeekDay = RTC_WEEKDAY_TUESDAY;

    if (HAL_RTC_SetDate(&RtcHandle, &sdatestructure, FORMAT_BCD) != HAL_OK) {
        Error_Handler();
    }

    // Set Time: 02:20:00
    stimestructure.Hours = 0x02;
    stimestructure.Minutes = 0x20;
    stimestructure.Seconds = 0x00;
    stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
    stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE ;
    stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

    if (HAL_RTC_SetTime(&RtcHandle, &stimestructure, FORMAT_BCD) != HAL_OK) {
        Error_Handler();
    }
}
*/

static void CheckButtonThread(void const * argument) {
    for (;;) {
        if (MatrixKeyboard_scan(&mainKeyboard)) {
            vTaskDelay(20);
            MatrixKeyboard_scan(&mainKeyboard);
        }
        MatrixKeyboard_updateEvents(&mainKeyboard);

        Menu_processKeyboard(&mainKeyboard);

        /*
        if (MatrixKeyboard_getPressedKeysCount(&mainKeyboard) == 1) {
            // F1 key down
            if (MatrixKeyboard_getKeyEvent(&mainKeyboard, &MATRIX_KEYBOARD_4X4_T1_KEY_F1) == MATRIX_KEYBOARD_KEY_DOWN) {
                if (screen != SCREEN_MENU) {
                    screen = SCREEN_MENU;
                    selectedMenu = MENU_AIR1;
                }
                else {
                    if (selectedMenu == MENU_AIR1 || selectedMenu == MENU_AIR2) {
                        selectedMenuParameter++;
                        if (selectedMenuParameter > 1) {
                            selectedMenuParameter = 0;
                            selectedMenu++;
                        }
                    }
                    else {
                        selectedMenu++;
                        selectedMenuParameter = 0;
                    }

                    // Reset to the first menu screen
                    if (selectedMenu > MENU_HEAT) {
                        selectedMenu = 0;
                        selectedMenuParameter = 0;
                    }
                }
            }
            // ESC key down
            else if (MatrixKeyboard_getKeyEvent(&mainKeyboard, &MATRIX_KEYBOARD_4X4_T1_KEY_ESC) == MATRIX_KEYBOARD_KEY_DOWN) {
                // Return to main screen
                screen = SCREEN_MAIN;
            }
            // DO key down
            else if (MatrixKeyboard_getKeyEvent(&mainKeyboard, &MATRIX_KEYBOARD_4X4_T1_KEY_DO) == MATRIX_KEYBOARD_KEY_DOWN) {
                if (screen == SCREEN_MENU) {
                    // Save parameters
                    screen = SCREEN_MAIN;
                }
            }
            // TAB key down
            else if (MatrixKeyboard_getKeyEvent(&mainKeyboard, &MATRIX_KEYBOARD_4X4_T1_KEY_TAB) == MATRIX_KEYBOARD_KEY_DOWN) {
                if (screen == SCREEN_MENU) {
                    if (selectedMenu == MENU_AIR1 || selectedMenu == MENU_AIR2) {
                        selectedMenuParameter++;
                        if (selectedMenuParameter > 1) {
                            selectedMenuParameter = 0;
                        }
                    }
                    else if (selectedMenu == MENU_HEAT) {

                    }
                }
            }
            // Num 2 key down
            else if (MatrixKeyboard_getKeyEvent(&mainKeyboard, &MATRIX_KEYBOARD_4X4_T1_KEY_2) == MATRIX_KEYBOARD_KEY_DOWN) {
                if (screen == SCREEN_MENU) {
                    switch (selectedMenu) {
                        case MENU_AIR1:
                            if (selectedMenuParameter == 0) {
                                configuration.air1T += 1;
                                Configuration_validate(&configuration, 0);
                            }
                            else if (selectedMenuParameter == 1) {
                                configuration.air1Delta += 1;
                                Configuration_validate(&configuration, 0);
                            }
                            break;

                        case MENU_AIR2:
                            if (selectedMenuParameter == 0) {
                                configuration.air2T += 1;
                                Configuration_validate(&configuration, 0);
                            }
                            else if (selectedMenuParameter == 1) {
                                configuration.air2Delta += 1;
                                Configuration_validate(&configuration, 0);
                            }
                            break;

                        case MENU_HEAT:
                            if (selectedMenuParameter == 0) {
                                configuration.heatSteps += 1;
                                Configuration_validate(&configuration, 0);
                            }
                            break;
                    }
                }
            }
            // Num 8 key down
            else if (MatrixKeyboard_getKeyEvent(&mainKeyboard, &MATRIX_KEYBOARD_4X4_T1_KEY_8) == MATRIX_KEYBOARD_KEY_DOWN) {
                if (screen == SCREEN_MENU) {
                    switch (selectedMenu) {
                        case MENU_AIR1:
                            if (selectedMenuParameter == 0) {
                                configuration.air1T -= 1;
                                Configuration_validate(&configuration, 0);
                            }
                            else if (selectedMenuParameter == 1) {
                                configuration.air1Delta -= 1;
                                Configuration_validate(&configuration, 0);
                            }
                            break;

                        case MENU_AIR2:
                            if (selectedMenuParameter == 0) {
                                configuration.air2T -= 1;
                                Configuration_validate(&configuration, 0);
                            }
                            else if (selectedMenuParameter == 1) {
                                configuration.air2Delta -= 1;
                                Configuration_validate(&configuration, 0);
                            }
                            break;

                        case MENU_HEAT:
                            if (selectedMenuParameter == 0) {
                                configuration.heatSteps -= 1;
                                Configuration_validate(&configuration, 0);
                            }
                            break;
                    }
                }
            }*/
        }
        vTaskDelay(20);

        /*
        if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != state) {
            vTaskDelay(20);
            newstate = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0);
            if ( newstate != state ) {
                state = newstate;
                if (newstate) {
                    Console_log("AT");

                    if (HAL_UART_Receive_DMA(&huart1, (uint8_t *)&uartRxChar, 1) != HAL_OK) {
                        Error_Handler();
                    }

                    char buffer[10];
                    if (HAL_UART_Receive(&huart1, buffer, 10, 0x1FFFFFF) != HAL_OK) {
                        Console_log("Read error");
                    }
                    else {
                        Console_log(buffer);
                    }

                    ledState = !ledState;
                    //configuration.waterT += 1;
                    //Configuration_validate();

                    char heart[7] = {0x0,0xa,0x1f,0x1f,0xe,0x4,0x0};
                    LCD_create_char(&hi2c1, 1, heart);
                    LCD_buffer_print(&hi2c1, 0, 8, buf);
                    LCD_refresh(&hi2c1);



                    //unsigned char buffer = LCD_BACKLIGHT;
                    //HAL_I2C_Master_Transmit(&hi2c1, 0x68, &buffer, 1, 10000);
                    //HAL_I2C_Master_Transmit(&hi2c1, 0x20, &buffer, 1, 1000);
                    //if (result != HAL_OK) {
                    //    buffer = 1;
                    //}


                    unsigned char rxbuffer[10];
                    if (HAL_UART_Receive(&huart1, rxbuffer, 3, 0x1FFFFFF) != HAL_OK) {
                        Error_Handler();
                    }


                    //if (HAL_UART_Transmit(&huart1, rxbuffer, 10, 5000) != HAL_OK) {
                    //    Error_Handler();
                    //}
                }
            }
        }
    }*/
}

static void OneWireThread(void const * argument) {
    while (1) {

        // Scan 1-Wire bus
        OneWire_ScanResult_t oneWireScanResult = OneWire_ScanBus(&oneWireBus, oneWireDevices, ONE_WIRE_DEVICES_LIMIT);
        //OneWire_ScanBus(&oneWireBus, oneWireDevices, ONE_WIRE_DEVICES_LIMIT);
        if (oneWireScanResult.error == ONE_WIRE_SCAN_OK) {

        }

        HeaterControlThread(argument);
        vTaskDelay(1000);
    }
}

static void HeaterControlThread(void const *argument) {
    //while (1) {
        if (inThermometer->status == TMTR_THERMOMETER_STATUS_READY && inThermometer->value != TMTR_TEMPERATURE_UNDEFINED) {
            if (inThermometer->value < configuration.inT - configuration.inTDelta) {
                heatSteps = 1;
            }
            else if (inThermometer->value > configuration.inT) {
                heatSteps = 0;
            }
        }
        else {
            heatSteps = 0;
        }

        if (heatSteps > 0) {
            // Turn on
        }
        else {
            // Turn off
        }

        //vTaskDelay(1000);
    //}
}

/***************************************************************************************************
 * Error handlers
 **************************************************************************************************/
void Error_Handler(void) {

}

void HardFault_Handler(void) {

}

/***************************************************************************************************
 * Callbacks
 **************************************************************************************************/

/**
 * Tx Transfer completed callback
 *
 * @param  UartHandle: UART handle.
 *
 * @note   This example shows a simple way to report end of DMA Tx transfer, and you can add your
 * own implementation.
 *
 * @return None
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle) {
    if (UartHandle == &huart1) {
        // Set transmission flag: trasfer complete
        UartReady = SET;
    }
}


/**
 * Rx Transfer completed callback
 *
 * @param  UartHandle: UART handle
 *
 * @note   This example shows a simple way to report end of DMA Rx transfer, and
 *         you can add your own implementation.
 *
 * @return None
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle) {
    if (UartHandle == &huart1) {
        if (uartRxChar == '\r' || uartRxChar == '\n' || uartRxBufferCount >= UART_RX_BUFFER_SIZE) {
            //Console_writeString("\r\n");
            uartRxBuffer[uartRxBufferCount-1] = 0;

            //Console_log("Execute command");
            //Console_writeString("argonio@shell $ ");

            uartRxBufferCount = 0;
        }
        else if (uartRxChar == '\177' || uartRxChar == '\b') {
            if (uartRxBufferCount > 0) {
                uartRxBufferCount--;
                uartRxBuffer[uartRxBufferCount] = 0;
                uint8_t remove[7] = {'\033', '[', '1', 'D', '\033', '[', 'K'};
                //uint8_t remove[4] = {'\b', ''};
                //HAL_UART_Transmit(UartHandle, remove, 7, 1000);
                //Console_writeString(" ");
                //Console_writeChar(&uartRxChar);
            }
        }
        else {
            uartRxBuffer[uartRxBufferCount] = uartRxChar;
            uartRxBufferCount++;
            //Console_writeChar(&uartRxChar);
        }

        if (HAL_UART_Receive_DMA(&huart1, (uint8_t *)&uartRxChar, 1) != HAL_OK) {
            Error_Handler();
        }
    }
}

/**
 * UART error callbacks
 *
 * @param  UartHandle: UART handle
 *
 * @note   This example shows a simple way to report transfer error, and you can
 *         add your own implementation.
 *
 * @return None
 */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle) {
    Error_Handler();
}

/***************************************************************************************************
 * Interrupt handlers.
 **************************************************************************************************/

extern void xPortSysTickHandler(void);

void SysTick_Handler(void) {
  if (xTaskGetSchedulerState() != taskSCHEDULER_NOT_STARTED) {
      xPortSysTickHandler();
  }
  HAL_IncTick();
}

void RTC_IRQHandler(void) {
    HAL_RTC_AlarmIRQHandler(&RtcHandle);
}

/**
  * USART1 RX/TX DMA interrupt request handler.
  *
  * @Note   This function is redefined in "main.h" and related to DMA
  *         used for USART data transmission
  */
void DMA1_Channel2_3_IRQHandler(void) {
  HAL_DMA_IRQHandler(huart1.hdmatx);
  HAL_DMA_IRQHandler(huart1.hdmarx);
}
