#ifndef ARGONIO_STM32_DRIVERS_CONF_H_
#define ARGONIO_STM32_DRIVERS_CONF_H_

/**
 * Include STM32Cube HAL header file for your MCU.
 */
#include "stm32f0xx.h"

/***********************************************************************************************************************
 * Delay.
 **********************************************************************************************************************/

/**
 * Minimal delay in microseconds - it must be measured manually by Logic Analyzer for each MCU model
 * It depends on the overhead of function call and speed of the MCU pin voltage level change.
 *
 * STM32F051 - 2 us
 */
#define DELAY_MIN   2

/**
 * Microsecond delay coefficient - it must be measured manually by Logic Analyzer for each MCU model
 *
 * STM32F051 - 12
 */
#define DELAY_COEF  12

#endif /* ARGONIO_STM32_DRIVERS_CONF_H_ */

/***********************************************************************************************************************
 * One Wire
 **********************************************************************************************************************/

/**
 * Use external hardware pull up resistor (OPTIONAL).
 *
 * Comment it if not used.
 *
 * This mode requires and external pull up resistor to be attached to the 1-Wire
 * data line. Default recommended resistor is 4.7kOhm.
 *
 * If not enabled then MCU's internal weak resistor will be used.
 *
 * WARNING: it's highly recommended to enable internal pull up. If your external pull up burns then
 * you will be able to detect 1-wire absence with internal pull up.
 */
#define ONE_WIRE_USE_INTERNAL_PULL_UP

/**
 * Use 1-Wire Parasite Power mode (OPTIONAL).
 *
 * Comment it if not used.
 *
 * This mode requires an additional hardware MOSFET strong pull up (SPU).
 * The MOSFET's base should be attached to the appropriate GPIO pin defined by
 * ONE_WIRE_SPU_GPIO and ONE_WIRE_SPU_PIN.
 *
 * SPU must be enabled after some commands to power up parasite 1-Wire slaves.
 * For example after "CONVERT T" command for DS18B20 sensor during t_conv time.
 */
#ifndef ONE_WIRE_USE_INTERNAL_PULL_UP
    #define ONE_WIRE_USE_STRONG_PULL_UP
#endif

/***********************************************************************************************************************
 * Thermometer
 **********************************************************************************************************************/
/**
 * Measures to be skipped on startup.
 *
 * As usual first measure is not proper.
 */
#define TMTR_SKIP_MEASURES_COUNT      1

/**
 * Measures on startup to be taken to calculate AVG value.
 */
#define TMTR_BOOTSTRAP_MEASURES_COUNT 3

/***********************************************************************************************************************
 * LCM1602 LCD
 **********************************************************************************************************************/
#define LCD_I2C_ADDRESS     0x4E //0x4F // 0x20
#define LCD_COLS              20
#define LCD_ROWS               4