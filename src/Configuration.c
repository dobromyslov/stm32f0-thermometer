#include "Configuration.h"
#include "../lib/Argonio/argonio-stm32-drivers/stm32f0xx-hal-eeprom-emu/stm32f0xx_hal_eeprom_emu.h"
#include "../lib/Argonio/argonio-stm32-drivers/OneWire/OneWire.h"

Configuration_t configuration;

typedef struct {
    uint8_t min;
    uint8_t max;
    uint8_t def;
    uint8_t minDelta;
    uint8_t defDelta;
} Temperature_Validator_t;

typedef struct {
    Temperature_Validator_t in;
    Temperature_Validator_t heater1;
    Temperature_Validator_t heater2;
} Validators_t;

Validators_t validators = {
        .in = {
                .min = 5,
                .max = 30,
                .def = 15,
                .minDelta = 1,
                .defDelta = 3
        },
        .heater1 = {
                .min = 5,
                .max = 40,
                .def = 35,
                .minDelta = 1,
                .defDelta = 3
        },
        .heater2 = {
                .min = 5,
                .max = 40,
                .def = 35,
                .minDelta = 1,
                .defDelta = 3
        }
};

uint16_t __readRom(uint8_t virtAddressStart, uint8_t rom[8]) {
    uint8_t i;
    uint16_t result;

    for (i = 0; i < 4; i++) {
        result = HAL_EEPROMEMU_ReadVariable(virtAddressStart + i, (uint16_t*)&rom[i * 2]);
        if (result == HAL_EEPROMEMU_NO_VALID_PAGE) {
            return HAL_EEPROMEMU_NO_VALID_PAGE;
        }
    }
}

uint16_t __updateRom(uint8_t virtAddressStart, uint8_t rom[8]) {
    uint8_t i;
    uint16_t result;

    for (i = 0; i < 4; i++) {
        result = HAL_EEPROMEMU_UpdateVariable(virtAddressStart + i, *((uint16_t*)&rom[i * 2]));
        if (result != HAL_OK) {
            return result;
        }
    }
}

uint16_t Configuration_init() {
    uint16_t result = HAL_EEPROMEMU_Init();
    if (result != HAL_OK) {
        return result;
    }

    // Load inT and inTDelta
    result = HAL_EEPROMEMU_ReadVariable(0, (uint16_t*)&configuration.inT);
    if (result == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    // Load heater1T and heater1Delta
    result = HAL_EEPROMEMU_ReadVariable(1, (uint16_t*)&configuration.heater1T);
    if (result == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    // Load heater2T and heater2Delta
    result = HAL_EEPROMEMU_ReadVariable(2, (uint16_t*)&configuration.heater2T);
    if (result == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    // Load heater1Enable and heater2Enable
    result = HAL_EEPROMEMU_ReadVariable(3, (uint16_t*)&configuration.heater1Enable);
    if (result == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    // Load inRom from {4, 5, 6, 7} address
    result = __readRom(4, configuration.inRom);
    if (result == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    // Load heater1Rom from {8, 9, 10, 11} address
    result = __readRom(8, configuration.heater1Rom);
    if (result == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    // Load heater2Rom from {12, 13, 14, 15} address
    result = __readRom(12, configuration.heater2Rom);
    if (result == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    // Load outRom from {16, 17, 18, 19} address
    result = __readRom(16, configuration.outRom);
    if (result == HAL_EEPROMEMU_NO_VALID_PAGE) {
        return HAL_EEPROMEMU_NO_VALID_PAGE;
    }

    configuration.heaterEnable = 0;

    if (!Configuration_validate(1)) {
        return HAL_ERROR;
    }
    Configuration_save(configuration);
}

void __validateTemperature(uint8_t *t, uint8_t *delta, Temperature_Validator_t validator, uint8_t toDefault) {
    if (*t - validator.minDelta < validator.min) {
        *t = toDefault ? validator.def : validator.min + validator.minDelta;
    }
    else if (*t > validator.max) {
        *t = toDefault ? validator.def : validator.max;
    }

    if (*delta < validator.minDelta) {
        *delta = toDefault ? validator.defDelta : validator.minDelta;
    }

    if (*t - *delta < validator.min) {
        *delta = toDefault ? validator.defDelta : *t - validator.min;
    }
}

void __validateRom(uint8_t rom[8]) {
    if (!OneWire_ValidateRomCRC(rom)) {
        memset(rom, 0, 8);
    }
}

void __validateEnableFlag(uint8_t *enable) {
    if (*enable < 0 || *enable > 1) {
        *enable = 0;
    }
}

uint8_t Configuration_validate(uint8_t toDefault) {
    __validateTemperature(&configuration.inT, &configuration.inTDelta, validators.in, toDefault);
    __validateTemperature(&configuration.heater1T, &configuration.heater1Delta, validators.heater1, toDefault);
    __validateTemperature(&configuration.heater2T, &configuration.heater2Delta, validators.heater2, toDefault);

    __validateEnableFlag(&configuration.heaterEnable);
    __validateEnableFlag(&configuration.heater1Enable);
    __validateEnableFlag(&configuration.heater2Enable);

    __validateRom(configuration.inRom);
    __validateRom(configuration.heater1Rom);
    __validateRom(configuration.heater2Rom);
    __validateRom(configuration.outRom);

    return 1;
}

uint16_t Configuration_save() {
    if (!Configuration_validate(0)) {
        return HAL_ERROR;
    }

    uint16_t result = HAL_EEPROMEMU_UpdateVariable(0, *((uint16_t*)(&configuration.inT)));
    if (result != HAL_OK) {
        return result;
    }

    result = HAL_EEPROMEMU_UpdateVariable(1, *((uint16_t*)(&configuration.heater1T)));
    if (result != HAL_OK) {
        return result;
    }

    result = HAL_EEPROMEMU_UpdateVariable(2, *((uint16_t*)(&configuration.heater2T)));
    if (result != HAL_OK) {
        return result;
    }

    result = HAL_EEPROMEMU_UpdateVariable(3, *((uint16_t*)(&configuration.heater1Enable)));
    if (result != HAL_OK) {
        return result;
    }

    result = __updateRom(4, configuration.inRom);
    if (result != HAL_OK) {
        return result;
    }

    result = __updateRom(8, configuration.heater1Rom);
    if (result != HAL_OK) {
        return result;
    }

    result = __updateRom(12, configuration.heater2Rom);
    if (result != HAL_OK) {
        return result;
    }

    result = __updateRom(16, configuration.outRom);
    if (result != HAL_OK) {
        return result;
    }
}
