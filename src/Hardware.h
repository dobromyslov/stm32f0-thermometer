
#ifndef STM32F0_THERMOMETER_HARDWARE_H
#define STM32F0_THERMOMETER_HARDWARE_H

#include "stm32f0xx.h"

#include "../lib/Argonio/argonio-stm32-drivers/Thermometer/Thermometer.h"
#include "../lib/Argonio/argonio-stm32-drivers/LCM1602/LCM1602.h"
#include "../lib/Argonio/argonio-stm32-drivers/MatrixKeyboard/MatrixKeyboard_4x4_Type1.h"
#include "../lib/Argonio/argonio-stm32-drivers/Relay/Relay.h"
#include "../lib/Argonio/argonio-stm32-drivers/Console/Console.h"

/**
 * RTC.
 */
//#define USE_RTC_LSI
#define USE_RTC_LSE 1

#ifdef USE_RTC_LSI
    // Calibrate to 40kHz LSI (STM32F051C8)
    #define RTC_PREDIV_A 124
    #define RTC_PREDIV_S 319
#else
    // Calibrate to 32.768 kHz
    #define RTC_PREDIV_A 127
    #define RTC_PREDIV_S 255
#endif

extern RTC_HandleTypeDef RtcHandle;

/**
 * I2C1 bus.
 *
 * PB6      I2C1_SCL
 * PB7      I2C1_SDA
 */
extern I2C_HandleTypeDef  hi2c1;

/**
 * UART1 bus.
 *
 * PA9      USART1_TX
 * PA10     USART1_RX
 */
extern UART_HandleTypeDef huart1;
extern __IO ITStatus UartReady;
#define UART_RX_BUFFER_SIZE 256
extern unsigned short uartRxBufferCount;
extern char uartRxBuffer[UART_RX_BUFFER_SIZE];
extern char uartRxChar;

/**
 * 1-Wire bus.
 */
extern OneWire_Bus_t oneWireBus;

#define ONE_WIRE_DEVICES_LIMIT  4

extern OneWire_Device_t oneWireDevices[ONE_WIRE_DEVICES_LIMIT];
extern uint8_t oneWireDevicesCount;

/**
 * Thermometer devices.
 */
#define THERMOMETERS_COUNT  4

extern TMTR_Thermometer_t thermometers[THERMOMETERS_COUNT];

extern TMTR_Thermometer_t *inThermometer;
extern TMTR_Thermometer_t *outThermometer;
extern TMTR_Thermometer_t *heater1Thermometer;
extern TMTR_Thermometer_t *heater2Thermometer;

/**
 * Relays.
 */
#define RELAYS_COUNT 4

extern Relay_Device_t relays[RELAYS_COUNT];

/**
 * Matrix Keyboard
 */
extern MatrixKeyboard_Device_t mainKeyboard;

void Hardware_Init(void);

#endif //STM32F0_THERMOMETER_HARDWARE_H
